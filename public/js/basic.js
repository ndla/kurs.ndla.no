$(function() {
  // Check if we have an editor on the page
  if($('.content-editor').length) {
    window.addEventListener("message", receiveMessage, false);
    CKEDITOR.replace("content-editor",{height:"300px"}); 
    // Take snapshot of page contents
    $originalpage = [ $('#url').contents(), CKEDITOR.instances['content-editor'].getData(), $('input#title').contents(), $('input#hiddentype').val() ];
    
  	$("#packagetitle").change(function() {
  		$("#packagetitle_mirror").val($(this).val());
  	});
  	$("#hours").change(function() {
  		$("#packagehours_mirror").val($(this).val());
  	});
  	$("#minutes").change(function() {
  		$("#packageminutes_mirror").val($(this).val());
  	});
    //Set change listeners for required fields
    CKEDITOR.instances['content-editor'].on('change', function() {validateForm()});
    $("input#url").keyup(validateForm());
    $("input#url").on('input',function() {validateForm()});
    $("input#url").click(closeErrorMessage());
  } //End if
  // Hide loading animation when iframe is done loading
  if($("#embedded-content > iframe").length) {
    $(".iframe-loading").show();
    $("#embedded-content > iframe").load(function() {
      $(".iframe-loading").hide();
    });
  }
});

function showErrorMessage(msg)
{
  document.getElementById('error-msg').innerHTML = msg;
  $('#error-messages').slideDown();
}

function validateForm()
{
  if(pageIsEdited()) {
    
    $('.save').click(function() {showErrorMessage('Du har ulagrede endringer på din nåværende side. Lagre endringer før du kan lagre pakke og lukke.');return false; });
    setTimeout(function(){ 
      closeErrorMessage()
    },10000);
  } else {
  }
  if($('input#hiddentype').length){
    if((CKEDITOR.instances['content-editor'].getData() != "" || $("#url").val().length > 0 ) && $('input#hiddentype').val() != 0)
    { // For validated
      $("#url").attr("style", "border:1px solid #b5b5b5");
      $("a .page-types").attr("style", "border:1px solid #ededed");
      $('.submit').attr('onclick', 'window.onbeforeunload = null;');
      $('#content-editor-border').attr("style", "border:1px solid #b5b5b5");
      $(".submit").removeClass("inactive");
    }
    else
    {
      $('.submit, .save').attr('onclick', 'saveIssues();return false;');
      $(".submit").addClass("inactive");
    }
  }
}

function openContentBrowser()
{
  window.open(server + '/contentbrowser?output=json&oncomplete=postMessage');
}

function receiveMessage(event)
{
  if (event.origin !== server || !event.data.nid) return;

  /**
    * 'server' here is path to red, but then the nodes
    * are published the path will be http://ndla.no/node/123
    * or if used with test environment http://cm.test.ndla.no/node/123
    */
  $('#url').val(server + '/node/' + event.data.nid);
  $('#title').val(event.data.title);
  if($("#url").val().length > 0 && $('input#hiddentype').val() != 0)
  {
    $("#url").attr("style", "border:1px solid #b5b5b5");
    $(".page-types").attr("style", "border:1px solid #b5b5b5");
    $('.submit').attr('onclick', 'window.onbeforeunload = null;');
    $(".submit").removeClass("inactive");
  }
}

function closeErrorMessage()
{
  $('#error-messages').slideUp();
}
function confirmDelete()
{
  $('#confirmdelete').slideDown();
}
function cancelDelete()
{
  $('#confirmdelete').slideUp();
}
function slideHelpUp(cl)
{
  if($('.tips-box').css('display') == 'block')
  {
    $('.tips-box').slideUp();
    $('.package-edit-menu .help').removeClass('active');
  }
  else
  {
    $('.tips-box').slideDown();
    $('.package-edit-menu .help').addClass('active');
  }
}
function toggleForm()
{
  if($(".form-hider").css('display') == 'block')
  {
    $(".form-hider").slideUp();
    document.getElementById('slider-bar').innerHTML = "Vis &nbsp;<i class='fa fa-chevron-down' style='font-size:1.3em;'></i>";
  }
  else
  {
    $(".form-hider").slideDown();
    document.getElementById('slider-bar').innerHTML = "Skjul &nbsp;<i class='fa fa-chevron-up' style='font-size:1.3em;'></i>";
  }
}

function saveIssues()
{
  if($('input#hiddentype').val() == 0)
  {
    showErrorMessage('Du må velge sidetype før du kan lagre siden.');
    //return false;
    $('a .page-types').attr('style', 'border:#fd7151 1px solid');
    return;
  }
  if(!CKEDITOR.instances['content-editor'].getData() && $('input#url').val().length == 0)
  {
    showErrorMessage('Du prøver å lagre en side uten innhold. Vi trenger minimum innhold i tekstfelt eller URL-felt. Du kan eventuelt slette denne siden.');
    $('#content-editor-border').attr('style', 'border:#fd7151 1px solid');
    $('input#url').attr('style', 'border:#fd7151 1px solid');
    return;
  }
}

function slideCloseUp(cl)
{
	if($('.'+cl).css('display') == 'block')
		$('.'+cl).slideUp();
	else
		$('.'+cl).slideDown();
}

function setActivePageTypeInForm(num, o)
{
	$('.page-type-box a').removeClass('active');
	$(o).addClass('active');
	$('#hiddentype').val(num);
	$('.page-types').attr('style', 'border:1px solid #ededed;');
	if($('input#url').val().length > 0 || CKEDITOR.instances['content-editor'].getData())
	{
		$(".submit").removeClass("inactive");
		$('.submit').attr('onclick', 'window.onbeforeunload = null;');
	}
	closeErrorMessage();
}

function pageIsEdited()
{
  $snapshot = [ $('#url').contents(), CKEDITOR.instances['content-editor'].getData(), $('input#title').contents(), $('input#hiddentype').val() ];
  if($snapshot.toString() !== $originalpage.toString())
    return true
  return false
}