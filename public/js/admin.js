(function($) {
  $(function() {
    $delete_buttons = $('.js-delete');
    $.each($delete_buttons, function(i, el) {
        $(el).on('click', function(e) {
            e.preventDefault();
            $.ajax({
                url: "/admin/delete/" + $(e.target).data('id'),
                success: function(data) {
                    $(e.target).parents('tr').remove();
                },
                dataType: "json"
            });
        });
    });

    $publish_unpublish_buttons = $('.js-publish-unpublish');

    $.each($publish_unpublish_buttons, function(i, el) {
        $(el).on('click', function(e) {
            e.preventDefault();
            var action = 'unpublish';
            if($(e.target).data('action') == 'publish') {
                action = 'publish';
            } 
            $.ajax({
                url: "/admin/" + action + "/" + $(e.target).data('id'),
                success: function(data) {
                    $(e.target).closest('.js-publish-td').find('.js-publish-unpublish').parent().toggleClass('hide');
                },
                dataType: "json"
            });
        });
    });
  })
})(jQuery)