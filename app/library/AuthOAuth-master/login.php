<?php

require(__DIR__.'/example.php');

/*
 * TODO / Notice - The state parameter should be protected by some sort of MAC.
	 * It is usually used for storing any required state that needs to be kept
	 * for the return endpoint (callback from the external oauth service) after
	 * authorization at the oauth service.
 */
$oauth->setState($_GET['redirect']);

header('Location: '.$oauth->getAuthorizeUrl());