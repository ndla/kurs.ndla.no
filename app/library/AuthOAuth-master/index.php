<title>OAuth test</title>
<h1>OAuth test</h1>
<?php
if (!session_id()) {
	session_start();
}
if (isset($_SESSION['identity'])) {
	?>
	<p>Logged in as <?php echo htmlspecialchars($_SESSION['identity']); ?>. <a href="<?php echo htmlspecialchars('/logout.php?redirect=' . rawurldecode($_SERVER['REQUEST_URI'])); ?>">Logout</a></p>
	<?php
} else {
	?>
	<p>
		<a href="<?php echo htmlspecialchars('/login.php?redirect=' . rawurldecode($_SERVER['REQUEST_URI'])); ?>">Login</a>
	</p>
	<?php
}
