<?php

require(__DIR__.'/example.php');

$identity = $oauth->getIdentity();

if (!session_id()) {
	session_start();
}

$_SESSION['identity'] = $identity->getIdentity();

$redirect = $oauth->getState();

header('Location: '.$redirect);
