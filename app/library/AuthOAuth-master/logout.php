<?php

if (!session_id()) {
	session_start();
}
unset($_SESSION['identity']);

header('Location: '.$_GET['redirect']);
