<?php

require(__DIR__.'/authoauth.php');

$config = require(__DIR__.'/exampleconfig.php');

$oauth = new no\seria\authoauth\AuthOAuth($config['hostUri'], $config['oauthId'], $config['oauthKey']);
$oauth->setRedirectUri($config['redirectUri']);
