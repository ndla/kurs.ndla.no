<?php

namespace no\seria\authoauth {
	class Identity {
		private $identity;

		public function __construct($identity) {
			$this->identity = $identity;
		}
		public static function fromJson($json) {
			$data = json_decode($json, TRUE);
			$obj = new Identity($data['identityId']);
			return $obj;
		}
		public function getIdentity() {
			return $this->identity;
		}
	}
}