<?php

namespace no\seria\authoauth {
	class AccessToken
	{
		private $value;
		private $tokenType;
		private $expires;
		private $refreshToken;
		private $scope;

		public function __construct($value, $tokenType, $expires, $refreshToken, $scope) {
			$this->value = $value;
			$this->tokenType = $tokenType;
			$this->expires = $expires;
			$this->refreshToken = $refreshToken;
			$this->scope = $scope;
		}

		public static function fromJson($json) {
			$data = json_decode($json, TRUE);
			$expiresTm = time() + $data['expires_in'] - 10 /* network delay comp. */;
			$expires = new \DateTime(date('Y-m-d H:i:s', $expiresTm));
			return new AccessToken($data['access_token'], $data['token_type'], $expires, $data['refresh_token'], $data['scope']);
		}

		public function getAccessToken() {
			return $this->value;
		}
	}
}