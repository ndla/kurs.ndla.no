<?php

namespace {
	require_once(__DIR__.'/AccessToken.php');
	require_once(__DIR__.'/Identity.php');
}

namespace no\seria\authoauth {
	class AuthOAuth
	{
		private $hostUrl;
		private $oauthId;
		private $oauthKey;

		private $redirectUri;
		private $scope;
		private $state;

		private $accessToken = null;

		public static function getServerBaseUrl()
		{
			$https = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off');
			return ($https ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'];
		}

		public function __construct($hostUrl, $oauthId, $oauthKey)
		{
			$this->hostUrl = $hostUrl;
			$this->oauthId = $oauthId;
			$this->oauthKey = $oauthKey;

			$this->redirectUri = AuthOAuth::getServerBaseUrl() . '/oauth2callback';
			$this->scope = array('read');
			$this->state = NULL;
		}

		public function pathToUrl($path)
		{
			if (!$this->hostUrl)
				throw new Exception("Must have a host url for the oauth service");
			$hostUrl = $this->hostUrl;
			if (substr($hostUrl, -1) != '/')
				$hostUrl .= '/';
			if ($path && substr($path, 0, 1) == '/') {
				$path = substr($path, 1);
			}
			return $hostUrl . $path;
		}

		public function getAuthorizeEndpointUrl()
		{
			return $this->pathToUrl("/oauth/authorize");
		}

		public function getAccessTokenEndpointUrl()
		{
			return $this->pathToUrl("/oauth/token");
		}

		public function getIdentityEndpointUrl() {
			return $this->pathToUrl("/v1/identity");
		}

		public function setRedirectUri($redirectUri)
		{
			$this->redirectUri = $redirectUri;
		}

		public function getRedirectUri()
		{
			return $this->redirectUri;
		}

		public function setScope(array $scope)
		{
			$this->scope = $scope;
		}

		public function getScope()
		{
			return $this->scope;
		}

		public function addToScope($scope)
		{
			if (!in_array($scope, $this->scope)) {
				$this->scope[] = $scope;
			}
		}

		public function removeFromScope($scope)
		{
			$key = array_search($scope, $this->scope);
			if ($key !== FALSE) {
				unset($this->scope[$key]);
			}
		}

		public function setState($state) {
			$this->state = $state;
		}

		public function getAuthorizationCode()
		{
			return isset($_GET['code']) ? $_GET['code'] : NULL;
		}
		public function getState() {
			return isset($_GET['state']) ? $_GET['state'] : $this->state;
		}

		public function getAuthorizeUrl()
		{
			$params = array(
				'client_id' => $this->oauthId,
				'redirect_uri' => $this->redirectUri,
				'response_type' => 'code',
			);
			if ($this->scope) {
				$scope = implode(' ', $this->scope);
				$params['scope'] = $scope;
			}
			if ($this->state !== null) {
				$params['state'] = $this->state;
			}
			$query = http_build_query($params, null, '&');
			return $this->getAuthorizeEndpointUrl() . '?' . $query;
		}

		/**
		 * @return AccessToken|null
		 */
		public function getAccessToken()
		{
			if ($this->accessToken !== null) {
				return $this->accessToken;
			}
			$params = array(
				'client_id' => $this->oauthId,
				'code' => $this->getAuthorizationCode(),
				'redirect_uri' => $this->redirectUri,
				'grant_type' => 'authorization_code',
			);
			if ($this->scope) {
				$scope = implode(' ', $this->scope);
				$params['scope'] = $scope;
			}
			$query = http_build_query($params, null, '&');
			$url = $this->getAccessTokenEndpointUrl() . '?' . $query;
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_USERPWD, $this->oauthId . ':' . $this->oauthKey);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			$content = curl_exec($ch);
			if ($content !== FALSE) {
				curl_close($ch);
				$this->accessToken = AccessToken::fromJson($content);
				return $this->accessToken;
			} else {
				curl_close($ch);
				return NULL;
			}
		}

		public function signUri($uri) {
			if (strpos($uri, '?') !== false) {
				$uri .= '&';
			} else {
				$uri .= '?';
			}
			$uri .= 'access_token='.rawurlencode($this->getAccessToken()->getAccessToken());
			return $uri;
		}

		public function getIdentity() {
			$url = $this->getIdentityEndpointUrl();
			$url = $this->signUri($url);
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			$content = curl_exec($ch);
			if ($content !== FALSE) {
				curl_close($ch);
				return Identity::fromJson($content);
			} else {
				curl_close($ch);
				return NULL;
			}
		}
	}
}
