<?php
use LaravelBook\Ardent\Ardent;
class PackageVersionPageAssoc extends Ardent
{
	protected $table = 'packageversions_pages';
	protected $guarded = array('id');
	protected $fillable = array('pos','page_id','package_version_id');

	public static $rules = array('pos' => 'required',
					'page_id' => 'required',
					'package_version_id' => 'required');

}
