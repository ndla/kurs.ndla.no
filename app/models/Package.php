<?php
use LaravelBook\Ardent\Ardent;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Package extends Ardent
{
        use SoftDeletingTrait;

        protected $table = 'packages';
        protected $softDelete = true;
        
	protected $guarded = array('id');
        protected $dates = ['deleted_at'];

	protected $fillable = array('package_version_id');
	public static $rules = array('package_version_id' => 'required');


        public function package_versions()
        {
                return $this->hasOne('PackageVersions', 'id', 'package_version_id');
        }

        
}
