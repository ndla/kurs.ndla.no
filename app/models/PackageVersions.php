<?php
use LaravelBook\Ardent\Ardent;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class PackageVersions extends Ardent
{
	use SoftDeletingTrait;
    protected $dates = ['deleted_at'];
	protected $softDelete = true;
	
	protected $table = 'package_versions';
	protected $guarded = array('id');
	protected $fillable = array('title','parent_id','creator_id', 'commited', 'hours', 'minutes');

	public static $rules = array('title' => 'required',
					'creator_id' => 'required');


					

	public function pages()
	{
		return $this->belongsToMany('Page', 'packageversions_pages', 'package_version_id', 'page_id')->withPivot('pos')->orderBy('pos');
	}
}
