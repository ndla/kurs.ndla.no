<?php
use LaravelBook\Ardent\Ardent;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Page extends Ardent
{
  use SoftDeletingTrait;
  protected $dates = ['deleted_at'];
  protected $softDelete = true;
  
  protected $table = 'pages';
  protected $guarded = array('id');
  protected $fillable = array('title', 'url', 'content', 'type', 'creator_id');
  public static $rules = array('title' => 'required', 'creator_id' => 'required', 'type' => 'required');

  public function packages() {
    return $this->belongsToMany('PackageVersions', 'packageversions_pages', 'package_version_id', 'page_id')->withPivot('pos')->orderBy('pos');
  }

  public function getEmbed() {
    //Get item from cache and return if found
    $value = Cache::get('Embed'.$this->url);
    if($value)
      return $value;
    //If url is to red, we need to add http authorization
    if(strpos($this->url,'red.ndla.no') || strpos($this->url,'red.test.ndla.no')) {
      $credentials = $_ENV['red-apache-login'];
      $config = [
        'resolver' => [
          'config' => [
            CURLOPT_HTTPHEADER => array("Authorization: Basic " . base64_encode($credentials)),
            CURLOPT_TIMEOUT => 20,
            CURLOPT_CONNECTTIMEOUT => 20
          ]
        ]
      ];
      $info = Embed\Embed::create($this->url,$config);
    } else {
    $info = Embed\Embed::create($this->url);
    }
    //Add item to cache before returning new embed object
    Cache::add('Embed'.$this->url, $info, 1440);
    return $info;
  }

  public static function scrape($url) {
    $meta = @get_meta_tags($url);
    $page = @file_get_contents($url);
    $titleS = strpos($page,'<title>')+7;
    $titleL = strpos($page,'</title>')-$titleS;
    $title = substr($page, $titleS, $titleL);
    return $title;
  }
}
