<?php
use LaravelBook\Ardent\Ardent;
class Users extends Ardent
{
	protected $table = 'users';
	protected $guarded = array('id');
	protected $fillable = array('externalId');

	public static $rules = array('title' => 'externalId');

}
