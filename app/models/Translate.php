<?php
class Translate
{
	private $jsonResult, $httpCode, $url;

	public function __construct($json)
	{
		$url = $_ENV['red-server'] . '/ndla-packages/translate';
		$this->url = $url;
		$this->fetchContent($json);
		if((int) $this->httpCode >= 400 || (int) $this->httpCode == 0 || strlen($this->jsonResult) == 0)
			return false;
	}

	public function fetchContent($json)
	{
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (compatible; SimpleScraper)');
		curl_setopt($ch, CURLOPT_URL, $this->url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30); // times out after 30s
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json));
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/json'));
		$this->jsonResult = curl_exec($ch);
		$info = curl_getinfo($ch);
		curl_close($ch);
		$this->httpCode = $info['http_code'];
	}

	public function getContent()
	{
		return json_decode($this->jsonResult);
/*		$pagesJSON = array();
		$pagesJSON[] = array('url'=>'http://google.com','pos'=>2,'type'=>2);
		$pagesJSON[] = array('url'=>'http://dailymail.co.uk','pos'=>3,'type'=>3);
		$pagesJSON[] = array('url'=>'http://google.co.uk','pos'=>4,'type'=>4);
		return json_encode($pagesJSON);*/
	}
}
