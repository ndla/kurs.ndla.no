@extends('layout')
@section('content')
  <header>
    <div class="main-bar grid-container">
      <div class="left-head mobile-grid-45 grid-45 tablet-grid-45  hide-right-gutter">
        <img class="logo" src="/img/header/logo.png" alt="To start page for Nasjonal digital læringsarena">
        <div class="slogan">Digitale læremidler for videregående opplæring</div>
      </div>
      <div class="right-head mobile-grid-55 grid-55 tablet-grid-55"></div>
    </div>
  </header>
  <div class="package-edit-menu">
    <div class="grid-container">
      <div class="mobile-grid-100 grid-100 page-title-main tablet-grid-100 hide-gutter-right">
        <div class="grid-80 tablet-grid-80 mobile-grid-80">&nbsp;
        </div>
        <a href="javascript:window.close();">
          <div class="save grid-20 tablet-grid-20 mobile-grid-20">
            Lukk læringssti
          </div>
        </a>
      </div>
    </div>
  </div>
	<div class="grid-container error-screen hide-gutter hide-gutter-right">
		<div id="error-messages" class="error-messages">
			@if(Session::has('message'))
				<div class="main-error" id="error-msg"><i class="fa fa-exclamation-triangle"></i> &nbsp;{{Session::get('message')}}</div>
			@else
				<div class="main-error" id="error-msg"><i class="fa fa-exclamation-triangle"></i> &nbsp;En uventet feil har oppstått</div>
			@endif
		</div>
	<div>
@stop
