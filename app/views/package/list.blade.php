@extends('layout')
@section('content')
	{{ link_to_route('package.create', 'Create a New Package') }}
	@foreach($package as $p)
		<br><a href="/packages/{{$p->id}}/page/1/edit">{{'id:'.$p->id . ' title:' . $p->title . ' parentId:'.$p->parent_id.' commited:'.$p->commited}}</a>
	@endforeach
@stop
