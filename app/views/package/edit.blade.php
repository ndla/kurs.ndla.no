@extends('layout')
@section('content')
	{{ HTML::script('js/ckeditor/ckeditor.js'); }}
	{{ HTML::script('js/admin.js'); }}
	{{ HTML::script('js/basic.js'); }}
	<script type="text/javascript">
		$(document).ready(function()
		{
			@if(Session::has('message'))
				@if($pos == 1)
					$('#content-editor-border').attr('style', 'border:#fd7151 1px solid');
				@else
					$('input#url').attr('style', 'border:#fd7151 1px solid');
				@endif
			@endif
			var fixHelper = function(e, ui)
			{
				ui.children().each(function()
				{
					$(this).width($(this).width());
				});
				return ui;
			};

			$('.left-area table tbody').sortable(
			{
				axis:"y",
				items: 'tr:not(.disabled)',
				placeholder:"ui-sortable-placeholder",
				start: function(e, ui)
					{
						ui.placeholder.height(ui.item.height());
						ui.placeholder.width(ui.item.width());
					},
				helper:fixHelper,
				update:function(event, ui)
				{
					movedItem = ui.item[0]["id"].substr(5);
					movedItem = movedItem.substr(0, movedItem.indexOf("-"));
					movedPos = ui.item[0]["id"].substr(ui.item[0]["id"].lastIndexOf("-")+1);
					if($("#"+ui.item[0]["id"]).next("tr").length == 0 || $("#"+ui.item[0]["id"]).next("tr")[0]["id"] == "add-new-page")
					{
						aboveItem = 0;
						abovePos = 0;
					}
					else
					{
						aboveItem = $("#"+ui.item[0]["id"]).next("tr")[0]["id"].substr(5, $("#"+ui.item[0]["id"]).next("tr")[0]["id"].lastIndexOf("-"));
						aboveItem = aboveItem.substr(0, aboveItem.indexOf("-"));
						abovePos = $("#"+ui.item[0]["id"]).next("tr")[0]["id"].substr($("#"+ui.item[0]["id"]).next("tr")[0]["id"].lastIndexOf("-")+1);
					}
					var jqxhr = jQuery.getJSON("/page/"+{{$package_id}}+"/"+movedItem+"/"+movedPos+"/"+aboveItem+"/"+abovePos+"/move", function(res)
					{
						if(typeof res["error"] !== "undefined")
						{
							console.log(res["error"]);
						}
						else
						{
							location.href = res["url"];
							window.onbeforeunload = null;
						}
					});
				}
			}).disableSelection();
            	window.onbeforeunload = function (e)
		{
                	var e = e || window.event;
			if($('input#url').val() != "{{$page->url}}" && $('input#url').length > 0)
			{
	                	//IE & Firefox
	               	 	if(e)
				{
	                	    e.returnValue = 'Du har ikke lagret endringene dine.';
	        	        }
				else
				{
	        	        	// For Safari
	        	            return 'Du har ikke lagret endringene dine.';
		            	}
			}
		};
{{--		@if($newpackage == 1)
			slideHelpUp();
			setTimeout(function(){slideHelpUp();}, 10000);
		@endif--}}
	});
	</script>
        <menu>
                <div class="grid-container menucontents">
                                <a href="javascript:slideHelpUp('tips-box');">
                                        <div class="mobile-grid-5 tablet-grid-5 grid-5 help">
       	                                        <i class="fa fa-question-circle"></i>
               	                        </div>
                                </a>
                </div>
        </menu>
{{Form::open(array('route' => array('package.commit', $package_id, $pos)))}}
	<div class="package-edit-menu">
		<div class="grid-container">
			<div class="mobile-grid-65 grid-65 page-title-main tablet-grid-65">
				{{htmlentities($page->title)}}
			</div>
			<div class="hide-gutter-right grid-35 tablet-grid-35 mobile-grid-35">
				<div class="grid-15 tablet-grid-15 mobile-grid-15">&nbsp;</div>
				<div class="grid-35 dropdown tablet-grid-35 mobile-grid-35 hide-gutter hide-gutter-right">
					{{Form::select('status', array('d' => 'Draft', 'p' => 'Published'))}}
				</div>
	{{--			<a href="javascript:slideHelpUp('tips-box');">
					<div class="grid-25 help tablet-grid-25 mobile-grid-25">
						<i class="fa fa-question-circle"></i>
					</div>
				</a>--}}
{{--				<a href="/packages/{{$package_id}}/page/{{$pos}}/save">
					<div class="save grid-50 tablet-grid-50 mobile-grid-50">
						Lagre og lukk
					</div>
				</a>--}}
					<input type="submit" value="Lagre og lukk" class="save tablet-grid-50 grid-50 mobile-grid-50">
			</div>
		</div>
	</div>
	<div class="package-properties-menu">
		<div class="package-properties grid-container">
			{{ Form::label('packagetitle', 'Tittel') }}
			{{ Form::text('packagetitle', $package->title) }}

			{{ Form::label('hours', 'Timer') }}
			{{Form::select('hours', array(0=>'0',1=>'1',2=>'2',3=>'3',4=>'4',5=>'5',6=>'6',7=>'7',8=>'8',9=>'9',10=>'10',11=>'11',12=>'12',
				13=>'13',14=>'14',15=>'15',16=>'16',17=>'17',18=>'18',19=>'19',20=>'20',21=>'21',22=>'22',23=>'23',24=>'24'), $package->hours)}}
			{{ Form::label('minutes', 'Minutter') }}
			{{Form::select('minutes', array(0=>'0',5=>'5',10=>'10',15=>'15',20=>'20',25=>'25',30=>'30',35=>'35',40=>'40',45=>'45',50=>'50',55=>'55'), $package->minutes)}}
		</div>
	</div>
</form>

<div class="grid-container">
{{ Form::model($page, array('route' => array('package.update', $page->id))) }}

	{{Form::hidden("packagetitle", $package->title, array('id' => 'packagetitle_mirror'))}}
	{{Form::hidden("packagehours", $package->hours, array('id' => 'packagehours_mirror'))}}
	{{Form::hidden("packageminutes", $package->minutes, array('id' => 'packageminutes_mirror'))}}

	@if(Session::has('message'))
		<div class="error-messages">
			<div>{{Session::get('message')}}</div>
		</div>
	@else
		<div id="error-messages" class="error-messages" style="display:none">
			<div id="error-msg"></div>
		</div>
	@endif
	<div class="grid-30 left-area hide-gutter tablet-grid-30 mobile-grid-30">
		<div class="top-nav-arrows grid-100 tablet-grid-100 mobile-grid-100">
			@if($page->pivot->pos < count($pages))
				<a href="/packages/{{$package_id}}/page/{{$page->pivot->pos + 1}}/preview">
					<div class="right-arrow"></div>
				</a>
			@endif
			@if($page->pivot->pos == count($pages))
				<div class="right-arrow no-pages"></div>
			@endif
			@if($page->pivot->pos ==  1)
				<div class="left-arrow no-pages"></div>
			@endif
			@if($page->pivot->pos >  1)
				<a href="/packages/{{$package_id}}/page/{{$page->pivot->pos - 1}}/preview">
					<div class="left-arrow"></div>
				</a>
			@endif
		</div>
		<div class="package-title grid-100 tablet-grid-100 mobile-grid-100">
			{{$package->title}}
		</div>
		<table cellpadding=0 cellspacing=0 border=0 class="left-menu">
			<thead><tr><th width="40"></th><th width="130"></th><th width="40"></th><th width="40"></th></thead>
		<tbody>
		<?php $finaltest = false; ?>
		@foreach($pages as $p)
			@if(strpos($p->title,"New Page") !== false)
				{{$p->title = "";}}
			@else
				<?php $p->title = htmlentities($p->title); ?>
			@endif
			@if($p->type != 8)
				@if($p->id == $page->id && $p->type == 1 && $p->pivot->pos == $pos)
					<tr class="active disabled" id="page-{{$p->id}}-{{$p->pivot->pos}}">
				@elseif($p->id == $page->id && $p->type != 1 && $p->pivot->pos == $pos)
					<tr class="active" id="page-{{$p->id}}-{{$p->pivot->pos}}">
				@elseif($p->type == 1)
					<tr class="disabled" id="page-{{$p->id}}-{{$p->pivot->pos}}">
				@else
					<tr id="page-{{$p->id}}-{{$p->pivot->pos}}">
				@endif
				<td class="page-icon-holder">
					@if($p->type == 0)
						<a href="/packages/{{$package_id}}/page/{{$p->pivot->pos}}/edit"><div class="page-icon blank grid-100 tablet-grid-100 mobile-grid-100"></div></a>
					@elseif($p->type == 1)
						<a href="/packages/{{$package_id}}/page/{{$p->pivot->pos}}/edit"><div class="page-icon intro grid-100 tablet-grid-100 mobile-grid-100"></div></a>
					@elseif($p->type == 2)
						<a href="/packages/{{$package_id}}/page/{{$p->pivot->pos}}/edit"><div class="page-icon page grid-100 tablet-grid-100 mobile-grid-100"></div></a>
					@elseif($p->type == 3)
						<a href="/packages/{{$package_id}}/page/{{$p->pivot->pos}}/edit"><div class="page-icon quiz grid-100 tablet-grid-100 mobile-grid-100"></div></a>
					@elseif($p->type == 4)
						<a href="/packages/{{$package_id}}/page/{{$p->pivot->pos}}/edit"><div class="page-icon task grid-100 tablet-grid-100 mobile-grid-100"></div></a>
					@elseif($p->type == 5)
						<a href="/packages/{{$package_id}}/page/{{$p->pivot->pos}}/edit"><div class="page-icon video grid-100 tablet-grid-100 mobile-grid-100"></div></a>
					@elseif($p->type == 6)
						<a href="/packages/{{$package_id}}/page/{{$p->pivot->pos}}/edit"><div class="page-icon summary grid-100 tablet-grid-100 mobile-grid-100"></div></a>
					@elseif($p->type == 7)
						<a href="/packages/{{$package_id}}/page/{{$p->pivot->pos}}/edit"><div class="page-icon test grid-100 tablet-grid-100 mobile-grid-100"></div></a>
					@else
						<a href="/packages/{{$package_id}}/page/{{$p->pivot->pos}}/edit"><div class="page-icon page grid-100 tablet-grid-100 mobile-grid-100"></div></a>
					@endif
				</td>
				@if($p->type == 1)
					@if(strlen($p->content) > 0)
						<td colspan="2" class="page-type-title"><a href="/packages/{{$package_id}}/page/{{$p->pivot->pos}}/preview"><div class="td_filler">{{$p->title}}</div></a></td>
					@else
						<td colspan="2" class="page-type-title"><a href="/packages/{{$package_id}}/page/{{$p->pivot->pos}}/preview"><div class="td_filler">{{$p->title}} &nbsp;<i class="fa fa-circle"></i> <span class="required">P&aring;krevd</span></div></a></td>
					@endif
					<td class="page-edit-holder"><a href="/packages/{{$package_id}}/page/{{$p->pivot->pos}}/edit"><div class="page-edit td_filler"><i class="fa fa-pencil"></i></div></a></td>
				@else
					<td class="page-type-title"><a href="/packages/{{$package_id}}/page/{{$p->pivot->pos}}/preview"><div class="td_filler">{{$p->title}}</div></a></td>
{{--					<td class="page-type-title">{{ HTML::linkRoute('package.edit', $p->title, array('packageId'=>$package_id, "pos"=>$p->pivot->pos)) }}</td>--}}
					<td class="page-edit-holder"><a href="/packages/{{$package_id}}/page/{{$p->pivot->pos}}/edit"><div class="page-edit td_filler"><i class="fa fa-pencil"></i></div></a></td>
					<td class="page-move-holder"><div class="page-move"><i class="fa fa-sort"></i></div></td>
				@endif
				</tr>
			@else
				<?php $finaltest = true;?>
				<tr class="disabled" id="add-new-page">
					<td class="page-icon-holder"><a href="/page/{{$package_id}}/create"><div class="page-icon add-page-icon"></div></a></td>
					<td colspan="3" class="page-type-title"><a href="/page/{{$package_id}}/create"><div class="td_filler">Legg til side</div></a></td>
				</tr>
				@if($p->id == $page->id)
					<tr class="disabled active" id="page-{{$p->id}}-{{$p->pivot->pos}}">
				@else
					<tr class="disabled" id="page-{{$p->id}}-{{$p->pivot->pos}}">
				@endif
					<td class="page-icon-holder">
						<a href="/packages/{{$package_id}}/page/{{$p->pivot->pos}}/edit"><div class="page-icon finaltest grid-100 tablet-grid-100 mobile-grid-100"></div></a>
					</td>
					@if(strlen($p->url) > 0)
						<td colspan="2" class="page-type-title"><a href="/packages/{{$package_id}}/page/{{$p->pivot->pos}}/preview"><div class="td_filler">{{$p->title}}</div></a></td>
					@else
						<td colspan="2" class="page-type-title"><a href="/packages/{{$package_id}}/page/{{$p->pivot->pos}}/preview"><div class="td_filler">{{$p->title}}{{-- &nbsp;<i class="fa fa-circle"></i> <span class="required">P&aring;krevd</span>--}}</div></a></td>
					@endif
					<td class="page-edit-holder"><a href="/packages/{{$package_id}}/page/{{$p->pivot->pos}}/edit"><div class="page-edit td_filler"><i class="fa fa-pencil"></i></div></a></td>
				</tr>

			@endif
		@endforeach
		@if(!$finaltest)
			<tr class="disabled" id="add-new-page">
				<td class="page-icon-holder"><a href="/page/{{$package_id}}/create"><div class="page-icon add-page-icon"></div></a></td>
				<td colspan="3" class="page-type-title"><a href="/page/{{$package_id}}/create"><div class="td_filler">Legg til side</div></a></td>
			</tr>
		@endif
		</tbody></table>
	</div>
	<div class="grid-70 right-area tablet-grid-70 mobile-grid-70">
		<div class="grid-100 tips-box tablet-grid-100 mobile-grid-100" style="display:none;">
			<div class="close">
				<a href="javascript:slideHelpUp('tips-box');">
					<i class="fa-times-circle-o fa"></i>
				</a>
			</div>
			<div class="info">
				<h2>Kom i gang!</h2>
				@if($page->type == 1)
{{--					<p>To create a package you must create an introduction page. The introduction page should give the student a brief summary of the course and what they will learn.</p>--}}
					<p>For &aring; opprette en pakke m&aring; du legge til en introduksjon og en avsluttende test. Introduksjonen b&oslash;r gi studenten en kort oppsummering av innholdet i pakken og &oslash;nsket l&aelig;ringsutbytte.</p>
				@else
{{--					<p>To add a page to your package simply copy and paste or browse to select the URL of the page you would like to add. The page title will automatically be added for you.</p>--}}
					<p>Innhold legges til enkelt ved &aring; lime inn URLen til siden/noden du vil bruke, eller ved &aring; bruke s&oslash;kfunksjonen til &aring; finne en node. Sidetittelen vil bli generert automatisk, men kan enkelt endres om du &oslash;nsker det.</p>
				@endif
			</div>
		</div>
		<div class="mobile-grid-100 grid-100 content-area tablet-grid-100">
		<div class="form-hider">
				{{ Form::hidden('package_id', $package_id) }}
				{{ Form::hidden('pos', $pos) }}
				@if($page->type == 1)
          @include('partials.intro', array(
              'page' => $page,
          ))
				@else
					{{ Form::hidden('pos', $pos) }}
					{{ Form::hidden('package_id', $package_id) }}
					<div class='form-field-box mobile-grid-100 grid-100 page-type-box tablet-grid-100'>
						{{ Form::hidden('type', $page->type, array('class'=>'hiddentype','id'=>'hiddentype')) }}
						{{-- page type --}}
						@if($page->type != 8)
							{{ Form::label('page_type', 'Sidetype') }}
							@if($page->type == 2)
								<a class="active" href="javascript:void(0);" onclick="setActivePageTypeInForm(2, this);">
							@else
								<a href="javascript:void(0);" onclick="setActivePageTypeInForm(2, this);">
							@endif
								<div class='page-types mobile-grid-15 grid-15 hide-gutter tablet-grid-15 hide-gutter-right'>
									<div class="page-type-icon text-page"></div>
									<div class="page-type-text">Fagstoff</div>
								</div>
							</a>
							@if($page->type == 3)
								<a class="active" href="javascript:void(0);" onclick="setActivePageTypeInFrom(3, this);">
							@else
								<a href="javascript:void(0);" onclick="setActivePageTypeInForm(3, this);">
							@endif
								<div class='page-types mobile=grid-15 grid-15 hide-gutter hide-gutter-right tablet-grid-15'>
									<div class="page-type-icon quiz-page"></div>
									<div class="page-type-text">Quiz</div>
								</div>
							</a>
							@if($page->type == 4)
								<a class="active" href="javascript:void(0);" onclick="setActivePageTypeInForm(4, this);">
							@else
								<a href="javascript:void(0);" onclick="setActivePageTypeInForm(4, this);">
							@endif
								<div class='page-types mobile-grid-15 grid-15 hide-gutter hide-gutter-right tablet-grid-15'>
									<div class="page-type-icon task-page"></div>
									<div class="page-type-text">Oppgave</div>
								</div>
							</a>
							@if($page->type == 5)
								<a class="active" href="javascript:void(0);" onclick="setActivePageTypeInForm(5, this);">
							@else
								<a href="javascript:void(0);" onclick="setActivePageTypeInForm(5, this);;">
							@endif
								<div class='page-types mobile-grid-15 grid-15 hide-gutter hide-gutter-right tablet-grid-15'>
									<div class="page-type-icon video-page"></div>
									<div class="page-type-text">Multimedia</div>
								</div>
							</a>
							@if($page->type == 6)
								<a class="active" href="javascript:void(0);" onclick="setActivePageTypeInForm(6, this);">
							@else
								<a href="javascript:void(0);" onclick="setActivePageTypeInForm(6, this);">
							@endif
								<div class='page-types mobile-grid-15 grid-15 hide-gutter hide-gutter-right tablet-grid-15'>
									<div class="page-type-icon summary-page"></div>
									<div class="page-type-text">Opp-<br>summering</div>
								</div>
							</a>
							@if($page->type == 7)
								<a class="active" href="javascript:void(0);" onclick="setActivePageTypeInForm(7, this);">
							@else
								<a href="javascript:void(0);" onclick="setActivePageTypeInForm(7, this);">
							@endif
								<div class='page-types last mobile-grid-15 grid-15 hide-gutter hide-gutter-right tablet-grid-15'>
									<div class="page-type-icon test-page"></div>
									<div class="page-type-text">Test</div>
								</div>
							</a>
					@endif
					</div>

					<div id='content-editor-url-page' class='form-field-box mobile-grid-100 grid-100 content-editor-container tablet-grid-100 hide-gutter'>
						{{ Form::label('content', 'Innhold', array('class' => 'editable')) }}
						<div id="content-editor-border">
							{{ Form::textarea('content', htmlspecialchars($page->content), array('class'=>'content-editor', 'id'=>'content-editor', 'name'=>'content-editor')) }}
						</div>
						<script>
							
					</script>
					</div>



					<div class='form-field-box mobile-grid-40 grid-40 hide-gutter hide-gutter-right tablet-grid-40'>
						{{-- url --}}
						{{ Form::label('url', 'URL', array('class' => 'editable')) }}
						{{ Form::text('url') }}
					</div>
					<div class='form-field-box content-browser-go mobile-grid-10 grid-10 hide-gutter tablet-grid-10' onClick="openContentBrowser();">
						{{ Form::label('&nbsp;','') }}
						@if($page->type == 8)
							<div class="browser-button" style="margin-top:2px;"><i class="fa fa-search"></i></div>
						@else
							<div class="browser-button"><i class="fa fa-search"></i></div>
						@endif
					</div>
					<div class='form-field-box mobile-grid-50 grid-50 page-title-input hide-gutter-right tablet-grid-50'>
						{{--title--}}
						{{ Form::label('title', 'Sidetittel', array('class' => 'not-editable')) }}
						{{ Form::text('title', $page->title) }}

						<div class='form-field-box mobile-grid-100 grid-100 hide-gutter-right button-box tablet-grid-100'>
							<div class='mobile-grid-45 grid-45 hide-gutter tablet-grid-45'>
								{{--submit--}}
								@if(strlen($page->url) == 0 && strlen($page->contents) == 0)
									{{ Form::submit('Lagre', array('class' => 'btn submit primary-button inactive')) }}
								@else
									{{ Form::submit('Lagre', array('class' => 'btn submit primary-button')) }}
								@endif
							</div>
							<div class='mobile-grid-40 grid-40 hide-gutter hide-gutter-right tablet-grid-40'>
								{{--cancel--}}
								{{ Form::reset('Angre', array('class' => 'btn cancel secondary-button')) }}
							</div>
							{{--@if($page->type != 8)--}}
								<div class='mobile-grid-15 grid-15  delete-button tablet-grid-15'>
									<button type="button"  onclick="confirmDelete();" class="btn tertiary-button delete"><i class="fa fa-trash"></i></button>
								</div>
							{{--@endif--}}
						</div>
          </div>
					</div>
						<div class="error-messages" id="confirmdelete" style="display:none;">
							<div style="float:left">Vil du virkelig slette siden? Sletting kan ikke reverseres</div>
							<div id="delete-confirm-button" onclick="cancelDelete();">Nei</div>
							<div id="delete-confirm-button" onclick="window.onbeforeunload = null;window.location.href='/page/{{$pos}}/package/{{$package_id}}/delete'">Ja</div>
						</div>
					<div style="clear:both;width:100%;height:10px;"></div>
					<div onClick="toggleForm();" id="slider-bar" class="slider-bar">
						Skjul &nbsp;<i class='fa fa-chevron-up' style='font-size:1.3em;'></i>
					</div>
					<div class="hidden-content external-style">
                        {{ $page->content }}
                        <div id='embedded-content'>
                          @if(isset($oembed))
                            @include('partials.display', array(
                               'page' => $page,
                               'oembed' => $oembed
                            ))
                          @endif
                        </div>
				@endif
		</div>
	</div>
</div>
		<div class="hide-gutter hide-gutter-right bottom-nav-arrows mobile-grid-70 grid-70 tablet-grid-70">
			<div class="inner">
			@if($page->pivot->pos > 1)
				<div class="mobile-grid-5 grid-5 tablet-grid-5">
					<a href="/packages/{{$package_id}}/page/1/preview">
						<div class="left-arrow-last"></div>
					</a>
				</div>
				<div class="mobile-grid-10 grid-10 tablet-grid-10">
					<a href="/packages/{{$package_id}}/page/{{$page->pivot->pos - 1}}/preview">
						<div class="left-arrow"></div>
					</a>
				</div>
			@else
				<div class="mobile-grid-5 grid-5 tablet-grid-5">
					<div class="left-arrow-last no-pages"></div>
				</div>
				<div class="mobile-grid-10 grid-10 tablet-grid-10">
					<div class="left-arrow no-pages"></div>
				</div>
			@endif
			<div class="mobile-grid-10 grid-10 tablet-grid-10">
				<div class="page-number">{{$page->pivot->pos}}/{{count($pages)}}</div>
			</div>
			@if($page->pivot->pos < count($pages))
				<div class="mobile-grid-10 grid-10 tablet-grid-10">
					<a href="/packages/{{$package_id}}/page/{{$page->pivot->pos + 1}}/preview">
						<div class="right-arrow"></div>
					</a>
				</div>
				<div class="mobile-grid-5 grid-5 tablet-grid-5">
					<a href="/packages/{{$package_id}}/page/{{count($pages)}}/preview">
						<div class="right-arrow-last"></div>
					</a>
				</div>
			@else
				<div class="mobile-grid-10 grid-10 tablet-grid-10">
					<div class="right-arrow no-pages"></div>
				</div>
				<div class="mobile-grid-5 grid-5 tablet-grid-5">
					<div class="right-arrow-last no-pages"></div>
				</div>
			@endif
			</div>
		</div>
{{ Form::close() }}
</div>
<script type="text/javascript">
@if($view == 1)
  $('.form-hider').css("display", "none");
  document.getElementById('slider-bar').innerHTML = "Vis &nbsp;<i class='fa fa-chevron-down' style='font-size:1.3em;'></i>";
@endif
</script>
@stop
