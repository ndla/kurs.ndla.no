@extends('layout')
@section('content')
<header>
	<div class="main-bar grid-container">
		<div class="left-head mobile-grid-45 grid-45 tablet-grid-45  hide-right-gutter">
			<img class="logo" src="/img/header/logo.png" alt="To start page for Nasjonal digital læringsarena">
            <div class="slogan">Digitale læremidler for videregående opplæring</div>
		</div>
		<div class="right-head mobile-grid-55 grid-55 tablet-grid-55"></div>
	</div>
</header>
	<div class="package-edit-menu">
		<div class="grid-container">
			<div class="mobile-grid-100 grid-100 page-title-main tablet-grid-100 hide-gutter-right">
				<div class="grid-80 tablet-grid-80 mobile-grid-80">
					{{--htmlentities($page->title)--}}
					Du er nå i en læringssti
				</div>
				<a href="javascript:window.close();">
                                        <div class="save grid-20 tablet-grid-20 mobile-grid-20">
                                                Lukk læringssti
                                        </div>
                                </a>
			</div>
		</div>
	</div>
<div class="grid-container student-view">
	@if(Session::has('message'))
		<div class="error-messages">
			<div>{{Session::get('message')}}</div>
		</div>
	@else
		<div id="error-messages" class="error-messages" style="display:none">
			<div id="error-msg"></div>
		</div>
	@endif
	<div class="mobile-grid-30 grid-30 left-area hide-gutter tablet-grid-30">
                <div class="top-nav-arrows mobile-grid-100 grid-100 tablet-grid-100">
                        @if($page->pivot->pos < count($pages))
                                <a href="/package/{{$package_id}}?page={{$page->pivot->pos + 1}}">
                                        <div class="right-arrow"></div>
                                </a>
                        @endif
                        @if($page->pivot->pos == count($pages))
                                <div class="right-arrow no-pages"></div>
                        @endif
                        @if($page->pivot->pos ==  1)
                                <div class="left-arrow no-pages"></div>
                        @endif
                        @if($page->pivot->pos >  1)
                                <a href="/package/{{$package_id}}?page={{$page->pivot->pos - 1}}">
                                        <div class="left-arrow"></div>
                                </a>
                        @endif
                </div>
		<div class="package-title grid-100 tablet-grid-100 mobile-grid-100">
                        {{$package->title}}
                </div>
		<table cellpadding=0 cellspacing=0 border=0 class="left-menu">
			<thead><tr><th width="40"></th><th width="130"></th><th width="40"></th><th width="40"></th></thead>
		<tbody>
		@foreach($pages as $p)
			@if(strpos($p->title,"New Page") !== false)
				{{$p->title = "";}}
			@else
				<?php $p->title = htmlentities($p->title); ?>
			@endif
			@if($p->pivot->pos == $pos && $p->id == $page->id)
				<tr id="page-{{$p->id}}-{{$p->pivot->pos}}" class="active">
			@else
				<tr id="page-{{$p->id}}-{{$p->pivot->pos}}">
			@endif
				<td class="page-icon-holder">
					@if($p->type == 0)
						<a href="/package/{{$package_id}}?page={{$p->pivot->pos}}"><div class="page-icon blank mobile-grid-100 grid-100 tablet-grid-100"></div></a>
					@elseif($p->type == 1)
						<a href="/package/{{$package_id}}?page={{$p->pivot->pos}}"><div class="page-icon intro mobile-grid-100 grid-100 tablet-grid-100"></div></a>
					@elseif($p->type == 2)
						<a href="/package/{{$package_id}}?page={{$p->pivot->pos}}"><div class="page-icon page mobile-grid-100 grid-100 tablet-grid-100"></div></a>
					@elseif($p->type == 3)
						<a href="/package/{{$package_id}}?page={{$p->pivot->pos}}"><div class="page-icon quiz mobile-grid-100 grid-100 tablet-grid-100"></div></a>
					@elseif($p->type == 4)
						<a href="/package/{{$package_id}}?page={{$p->pivot->pos}}"><div class="page-icon task mobile-grid-100 grid-100 tablet-grid-100"></div></a>
					@elseif($p->type == 5)
						<a href="/package/{{$package_id}}?page={{$p->pivot->pos}}"><div class="page-icon video mobile-grid-100 grid-100 tablet-grid-100"></div></a>
					@elseif($p->type == 6)
						<a href="/package/{{$package_id}}?page={{$p->pivot->pos}}"><div class="page-icon summary mobile-grid-100 grid-100 tablet-grid-100"></div></a>
					@elseif($p->type == 7)
						<a href="/package/{{$package_id}}?page={{$p->pivot->pos}}"><div class="page-icon test mobile-grid-100 grid-100 tablet-grid-100"></div></a>
					@elseif($p->type == 8)
						<a href="/package/{{$package_id}}?page={{$p->pivot->pos}}"><div class="page-icon finaltest mobile-grid-100 grid-100 tablet-grid-100"></div></a>
					@else
						<a href="/package/{{$package_id}}?page={{$p->pivot->pos}}"><div class="page-icon page mobile-grid-100 grid-100 tablet-grid-100"></div></a>
					@endif
				</td>
				<td colspan="3" class="page-type-title"><a href="/package/{{$package_id}}?page={{$p->pivot->pos}}"><div class="td_filler">{{$p->title}}</div></a></td>
			</tr>
		@endforeach
		</tbody>
	</table>
	</div>
	<div class="mobile-grid-70 grid-70 right-area tablet-grid-70">
		<div class="mobile-grid-100 grid-100 content-area tablet-grid-100">
		<div class="form-hider">
			@if($page->type == 1)
				<div class="hidden-content external-style">
					<h1>{{$page->title}}</h1>
					{{$page->content}}
				</div>
			@else
				<div class="hidden-content external-style">
                {{ $page->content }}
                <div id='embedded-content'>
                @if(isset($oembed))
                @include('partials.display', array(
                    'page' => $page,
                    'oembed' => $oembed
                ))
                @endif
                </div>
				</div>
            @endif
		</div>

	</div>
</div>
                <div class="bottom-nav-arrows mobile-grid-70 grid-70 hide-gutter hide-gutter-right tablet-grid-70">
                        <div class="inner">
                        @if($page->pivot->pos > 1)
                                <div class="mobile-grid-5 grid-5 tablet-grid-5">
                                        <a href="/package/{{$package_id}}?page=1">
                                                <div class="left-arrow-last"></div>
                                        </a>
                                </div>
                                <div class="mobile-grid-10 grid-10 tablet-grid-10">
                                        <a href="/package/{{$package_id}}?page={{$page->pivot->pos - 1}}">
                                                <div class="left-arrow"></div>
                                        </a>
                                </div>
                        @else
                                <div class="mobile-grid-5 grid-5 tablet-grid-5">
                                        <div class="left-arrow-last no-pages"></div>
                                </div>
                                <div class="mobile-grid-10 grid-10 tablet-grid-10">
                                        <div class="left-arrow no-pages"></div>
                                </div>
                        @endif
                        <div class="mobile-grid-10 grid-10 tablet-grid-10">
                                <div class="page-number">{{$page->pivot->pos}}/{{count($pages)}}</div>
                        </div>
                        @if($page->pivot->pos < count($pages))
                                <div class="mobile-grid-10 grid-10 tablet-grid-10">
                                        <a href="/package/{{$package_id}}?page={{$page->pivot->pos + 1}}">
                                                <div class="right-arrow"></div>
                                        </a>
                                </div>
                                <div class="mobile-grid-5 grid-5 tablet-grid-5">
                                        <a href="/package/{{$package_id}}?page={{count($pages)}}">
                                                <div class="right-arrow-last"></div>
                                        </a>
                                </div>
                        @else
                                <div class="mobile-grid-10 grid-10 tablet-grid-10">
                                        <div class="right-arrow no-pages"></div>
                                </div>
                                <div class="mobile-grid-5 grid-5 tablet-grid-5">
                                        <div class="right-arrow-last no-pages"></div>
                                </div>
                        @endif
                        </div>
                </div>

@stop

