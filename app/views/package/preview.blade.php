@extends('layout')
@section('content')
<script type="text/javascript">
        function slideHelpUp(cl)
        {
                if($('.tips-box').css('display') == 'block')
                {
                        $('.tips-box').slideUp();
                        $('.package-edit-menu .help').removeClass('active');
                }
                else
                {
                        $('.tips-box').slideDown();
                        $('.package-edit-menu .help').addClass('active');
                }
        }
</script>
	<div class="package-edit-menu">
		<div class="grid-container">
			<div class="grid-80 page-title-main">
				{{htmlentities($page->title)}}
			</div>
			<div class="hide-gutter-right grid-20">
				<a href="javascript:slideHelpUp('tips-box');">
					<div class="grid-25 help">
						<i class="fa fa-question-circle"></i>
					</div>
				</a>
				{{--<a href="/packages/{{$package_id}}/page/{{$pos}}/copy">--}}
				<a href="/package/{{$package_id}}/edit">
					<div class="save grid-75">
						Rediger Package
					</div>
				</a>
			</div>
		</div>
	</div>
<div class="grid-container">
	@if(Session::has('message'))
		<div class="error-messages">
			<div>{{Session::get('message')}}</div>
		</div>
	@else
		<div id="error-messages" class="error-messages" style="display:none">
			<div id="error-msg"></div>
		</div>
	@endif
	<div class="error-messages" id="confirmdelete" style="display:none;">
		<div style="float:left">Deleted pages cannot be recovered, are you sure you want to delete this page?</div>
		<div id="delete-confirm-button" onclick="cancelDelete();"><i class="fa fa-close"></i></div>
		<div id="delete-confirm-button" onclick="window.onbeforeunload = null;window.location.href='/page/{{$pos}}/package/{{$package_id}}/delete'"><i class="fa fa-check"></i></div>
	</div>
	<div class="grid-30 left-area hide-gutter">
		<table cellpadding=0 cellspacing=0 border=0 class="left-menu">
			<thead><tr><th width="40"></th><th width="130"></th><th width="40"></th><th width="40"></th></thead>
		<tbody>
		@foreach($pages as $p)
			@if(strpos($p->title,"New Page") !== false)
				{{$p->title = "";}}
			@else
				<?php $p->title = htmlentities($p->title); ?>
			@endif
			@if($p->type != 8)
				@if($p->id == $page->id && $p->type == 1 && $p->pivot->pos == $pos)
					<tr class="active disabled" id="page-{{$p->id}}-{{$p->pivot->pos}}">
				@elseif($p->id == $page->id && $p->type != 1 && $p->pivot->pos == $pos)
					<tr class="active" id="page-{{$p->id}}-{{$p->pivot->pos}}">
				@elseif($p->type == 1)
					<tr class="disabled" id="page-{{$p->id}}-{{$p->pivot->pos}}">
				@else
					<tr id="page-{{$p->id}}-{{$p->pivot->pos}}">
				@endif
				<td class="page-icon-holder">
					@if($p->type == 0)
						<a href="/packages/{{$package_id}}/page/{{$p->pivot->pos}}/preview"><div class="page-icon blank grid-100"></div></a>
					@elseif($p->type == 1)
						<a href="/packages/{{$package_id}}/page/{{$p->pivot->pos}}/preview"><div class="page-icon intro grid-100"></div></a>
					@elseif($p->type == 2)
						<a href="/packages/{{$package_id}}/page/{{$p->pivot->pos}}/preview"><div class="page-icon page grid-100"></div></a>
					@elseif($p->type == 3)
						<a href="/packages/{{$package_id}}/page/{{$p->pivot->pos}}/preview"><div class="page-icon quiz grid-100"></div></a>
					@elseif($p->type == 4)
						<a href="/packages/{{$package_id}}/page/{{$p->pivot->pos}}/preview"><div class="page-icon task grid-100"></div></a>
					@elseif($p->type == 5)
						<a href="/packages/{{$package_id}}/page/{{$p->pivot->pos}}/preview"><div class="page-icon video grid-100"></div></a>
					@elseif($p->type == 6)
						<a href="/packages/{{$package_id}}/page/{{$p->pivot->pos}}/preview"><div class="page-icon summary grid-100"></div></a>
					@elseif($p->type == 7)
						<a href="/packages/{{$package_id}}/page/{{$p->pivot->pos}}/preview"><div class="page-icon test grid-100"></div></a>
					@else
						<a href="/packages/{{$package_id}}/page/{{$p->pivot->pos}}/preview"><div class="page-icon page grid-100"></div></a>
					@endif
				</td>
				@if($p->type == 1)
					@if(strlen($p->content) > 0)
						<td colspan="3" class="page-type-title"><a href="/packages/{{$package_id}}/page/{{$p->pivot->pos}}/preview"><div class="td_filler">{{$p->title}}</div></a></td>
					@else
						<td colspan="3" class="page-type-title"><a href="/packages/{{$package_id}}/page/{{$p->pivot->pos}}/preview"><div class="td_filler">{{$p->title}} &nbsp;<i class="fa fa-circle"></i> <span class="required">Required</span></div></a></td>
					@endif
				@else
					<td colspan="3" class="page-type-title"><a href="/packages/{{$package_id}}/page/{{$p->pivot->pos}}/preview"><div class="td_filler">{{$p->title}}</div></a></td>
				@endif
				</tr>
			@else
				@if($p->id == $page->id)
					<tr class="disabled active" id="page-{{$p->id}}-{{$p->pivot->pos}}">
				@else
					<tr class="disabled" id="page-{{$p->id}}-{{$p->pivot->pos}}">
				@endif
					<td class="page-icon-holder">
						<a href="/packages/{{$package_id}}/page/{{$p->pivot->pos}}/preview"><div class="page-icon finaltest grid-100"></div></a>
					</td>
					@if(strlen($p->url) > 0)
						<td colspan="3" class="page-type-title"><a href="/packages/{{$package_id}}/page/{{$p->pivot->pos}}/preview"><div class="td_filler">{{$p->title}}</div></a></td>
					@else
						<td colspan="3" class="page-type-title"><a href="/packages/{{$package_id}}/page/{{$p->pivot->pos}}/preview"><div class="td_filler">{{$p->title}} &nbsp;<i class="fa fa-circle"></i> <span class="required">Required</span></div></a></td>
					@endif
				</tr>

			@endif
		@endforeach
		</tbody></table>
	</div>
	<div class="grid-70 right-area">
		<div class="grid-100 tips-box" style="display:none;">
			<div class="close">
				<a href="javascript:slideHelpUp('tips-box');">
					<i class="fa-times-circle-o fa"></i>
				</a>
			</div>
			<div class="info">
				<h2>Kom i gang!</h2>
				<p>For &aring; redigere pakken, velg 'Rediger Pakke'-knappen p&aring; verkt&oslash;ylinja.</p>
{{--				<p>To add a page to your package simply copy and paste or browse to select the URL of the page you would like to add. The page title will automatically be added for you.</p>--}}
			</div>
		</div>
		<div class="grid-100 content-area">
		<div class="form-hider">
			@if($page->type == 1)
				<div class="hidden-content external-style">
					<h1>{{$page->title}}</h1>
					{{$page->content}}
				</div>
			@else
				<div class="hidden-content external-style">
                {{ $page->content }}
                <div id='embedded-content'>
                  @if(isset($oembed))
                    @include('partials.display', array(
                      'page' => $page,
                      'oembed' => $oembed
                    ))
                  @endif
                </div>
				</div>
            @endif
		</div>
	</div>
</div>
@stop

