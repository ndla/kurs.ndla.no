@extends('admin')
@section('content')
{{ HTML::script('js/admin.js'); }}

<div style="margin-left: auto; margin-right: auto;" class='col11 flex flex-column items-end mt2'>
    <div class='btn bg-blue white p1 rounded'>
        @if(Input::get('show_deleted') == "true")
            <a class='white' href="?">Hide deleted</a>
        @else
            <a class='white' href="?show_deleted=true">Show deleted</a>
        @endif
    </div>
    <div class='mt1 btn bg-blue white p1 rounded'>
        <a class='white' href="/admin/logout">Logout</a>
    </div>
</div>


<div class='flex flex-column items-center mt2'>
    <table style='xtable-layout: fixed; border-collapse: collapse;' class='flex-shrink col11 flex-column black col12 p1 rounded'>
            <tr class='row head'>
                <td class="p1 border-bottom border-blue">ID</td>
                <td class="p1 border-bottom border-blue" style="width: 350px;" >Title</td>
                <td class="p1 border-bottom border-blue" >Published</td>
                <td class="p1 border-bottom border-blue">Created At</td>
                <td class="p1 border-bottom border-blue">Updated At</td>
                <td class="p1 border-bottom border-blue"></td>
                <td class="p1 border-bottom border-blue"></td>
            </tr>
        
        @foreach($packages as $i => $p)
            @if(!$p->trashed())
                <tr class='{{ $i % 2 ? 'bg-silver-50' : '' }}' style='height: 20px;'>
            @else
                <tr class='bg-red-50' style='height: 20px;'>
            @endif
                <td class="p1"><a href='{{ route("package.get", ['packageId' => $p->id]) }}'>{{$p->id}}</a></td>
                <td class="p1"><a href='{{ route("package.get", ['packageId' => $p->id]) }}'>{{$p->package_versions->title}}</a></td>
                <td class="p1 js-publish-td">
                
                    
                     <span class='{{ ($p->package_versions->commited == 0) ? '' : 'hide' }} h6 bg-red rounded white p1'>
                       <a data-action="publish" data-id="{{$p->id}}" href='/admin/publish/{{ $p->id }}' class='js-publish-unpublish white'>
                        <i class="fa fa-eye-slash" aria-hidden="true"></i>
                        Draft
                        </a>
                    </span>
                    
                     <span class='{{ ($p->package_versions->commited == 0) ? 'hide' : '' }} h6 bg-green rounded white p1'>
                        <a data-action="unpublish" data-id="{{$p->id}}" href='/admin/unpublish/{{ $p->id }}' class='js-publish-unpublish white'>
                            <i class="fa fa-eye" aria-hidden="true"></i> 
                            Published
                        </a>
                     </span>
                    
                    
                    </td>
                <td class="p1">{{$p->package_versions->created_at}}</td>
                <td class="p1">{{$p->package_versions->updated_at}}</td>
                <td class="p1">
                    
                    @if(!$p->trashed())
                    <span class='h6 bg-red rounded white p1'>
                        <a data-id="{{ $p->id }}" class='js-delete white' href="/admin/delete/{{ $p->id }}">
                            <i class="fa fa-times" aria-hidden="true"></i>
                            Remove
                        </a>
                    </span>
                    @else
                    <span class='h6 bg-black rounded white p1'>
                        <a class='white' href="/admin/restore/{{ $p->id }}">
                            <i class="fa fa-undo" aria-hidden="true"></i>
                            Restore
                        </a>
                    </span>
                    @endif
                </td>
                <td class="p1">
                    <span class='h6 bg-blue rounded white p1'>
                        <a class='white' href="package/{{$p->id}}/edit">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                            Edit
                            </a>
                    </span>
                </td>
            </tr>
        @endforeach
    </table>
</div>
@stop
