@extends('admin')
@section('title') Admin login @stop

@section('content')


<div class='flex flex-column items-center mt5 '>
    <div class='col3 bg-blue p1 pl3 pr3 pb2 rounded white'>

        @if ($errors->has())
            @foreach ($errors->all() as $error)
                <div class='bg-red white p1 alert rounded'>{{ $error }}</div>
            @endforeach
        @endif

        <h1 class='h2 p0 m0'>Login</h1>

        {{ Form::open(['role' => 'form']) }}

        <div class='form-group p0'>
            {{ Form::label('username', 'Username') }}
            <br />
            {{ Form::text('username', null, ['placeholder' => '', 'class' => 'form-control h3 border-box p0 pt1 pb1 col12']) }}
        </div>

        <div class='form-group mt2 p0 border-box'>
            {{ Form::label('password', 'Password') }}
            <br />
            {{ Form::password('password', ['placeholder' => '', 'class' => 'form-control h3 border-box  p0 pt1 pb1 col12']) }}
        </div>

        <div class='form-group'>
            {{ Form::submit('Login', ['class' => 'btn uppercase btn-primary bg-white h2 col12 mt3']) }}
        </div>

        {{ Form::close() }}
    </div>
</div>


@stop