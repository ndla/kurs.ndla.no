<div style="display:none;">
<div class='form-field-box tablet-grid-50 mobile-grid-50 grid-50 hide-gutter'>
  {{-- title --}}
  {{ Form::label('title', 'Sidetittel', array('class' => 'not-editable')) }}
  {{ Form::text('title', $page->title, array('disabled'=>'disabled', 'class'=>'disabled')) }}
</div>
<div class='form-field-box mobile-grid-50 grid-50 tablet-grid-50'>
  {{--page id--}}
  {{ Form::label('page_id', 'ID#', array('class' => 'not-editable')) }}
  {{ Form::text('id', $page->id, array('disabled'=>'disabled', 'class'=>'disabled')) }}
</div>
</div>
<div class='form-field-box mobile-grid-100 grid-100 content-editor-container tablet-grid-100 hide-gutter'>
  {{ Form::label('content', 'Innhold', array('class' => 'editable')) }}
  <div id="content-editor-border">
    {{ Form::textarea('content', htmlspecialchars($page->content), array('class'=>'content-editor', 'id'=>'content-editor', 'name'=>'content-editor')) }}
  </div>
</div>
<div class='form-field-box mobile-grid-100 grid-100 button-box tablet-grid-100'>
  <div class='mobile-grid-20 grid-20 hide-gutter tablet-grid-20'>
    {{--submit--}}
    {{ Form::submit('Lagre', array('class' => 'btn submit primary-button')) }}
  </div>
  <div class='mobile-grid-20 grid-20 tablet-grid-20'>
    {{--cancel--}}
    {{--{{ Form::reset('Angre', array('class' => 'btn cancel secondary-button')) }}--}}
  </div>
</div>
</div> <!-- end form-hider div !-->
<div onClick="toggleForm();" class="slider-bar" id="slider-bar">
  Skjul &nbsp;<i class='fa fa-chevron-up' style='font-size:1.3em;'></i>
</div>
<div class="hidden-content external-style">
  <h1>{{$page->title}}</h1>
  {{$page->content}}
