  <p class="iframe-loading">
    <i class="fa fa-cog fa-spin"></i> Laster side, vennligst vent...
  </p>
@if ($oembed->type == 'link')
<script>
	document._iframeHeight = 2000;
</script>
<iframe src="{{$oembed->url}}" allowfullscreen sandbox="allow-forms allow-pointer-lock allow-popups allow-scripts allow-same-origin"></iframe>

@elseif ($oembed->type == 'rich')
    <script>
        document._iframeHeight = {{$oembed->height}} + 200;

        addEventListener('message', function(m) {
            if(m.data.height) document._iframeHeight = m.data.height + 50;
        });
    </script>
	{{ $oembed->code }}

@elseif ($oembed->type == 'video')
	<h1>{{$oembed->title}}</h1>
	<script>
		document._iframeHeight = 200;
	</script>
	<script>
		setInterval(function() {
			var w = {{$oembed->width}}
			var h = {{$oembed->height}}
			var a = w/h;
			var ww = $("#embedded-content").width();
			document._iframeHeight = ww / a;
		}, 100);
	</script>
	{{ $oembed->code }}

@elseif ($oembed->type == 'photo')
	<h1>{{$oembed->title}}</h1>
	{{ $oembed->code }}
	<script>
		var w, h;
		// Force iframe size
		setInterval(function() {
			var $i = $("iframe");
			if(!w) {
				w = $i.width();
				h = $i.height();
			}
			var a = w/h;
			var ww = $(window).width();
			$i.css({width: ww, height: ww / a});
			document._iframeHeight = 50 + ww / a;
		}, 100);
	</script>

@else
	<script>
		document._iframeHeight = 50;
	</script>

	<p>Dette innholdet kan ikke vises her. <span style='color:white'>{{$oembed->type}}</span></p>
	<p><a href="{{$oembed->url}}" target="_blank">&Aring;pne siden i et nytt vindu.</a></p>

@endif

<script>
setIframeSize();
function setIframeSize() {
    window._oldHeight = 0;
    setInterval(function() {
      $iframe = $('#embedded-content iframe');
      var h = document._iframeHeight
        if($iframe && h) {
            $iframe = $("#embedded-content iframe");
            if(window._oldHeight < h) {
                // Content has grown
                window._oldHeight = h;
                $iframe.height(h + "px");
            } else {
                // Content has shrunk
                if(Math.abs(window._oldHeight - h) > 500) {
                    window._oldHeight = h;
                    $iframe.height(h + "px");
                }
            }
        }
    }, 100);
}
</script>
