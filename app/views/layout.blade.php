<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<!--		<meta name="viewport" content="width=device-width, initial-scale=1" />-->
<meta
    name="viewport"
    content="width=device-width, initial-scale=1, minimum-scale=1,maximum-scale=1"
  />
        <title>{{$package->title or 'NDLA Læringssti'}}</title>
		<link href='https://fonts.googleapis.com/css?family=Open+Sans:600|Open+Sans' rel='stylesheet' type='text/css'>
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		{{ HTML::script('js/jquery-1.11.1.min.js'); }}
		{{ HTML::script('js/jquery-ui-1.11.2.custom/jquery-ui.min.js'); }}
		{{ HTML::style('css/unsemantic-master/assets/stylesheets/unsemantic-grid-responsive-tablet.css'); }}
		{{ HTML::style('css/base.css'); }}
		{{ HTML::style('css/external.css'); }}
    <script>var server = '{{$_ENV['red-server']}}'</script>
	</head>
	<body>
{{--	<menu>
		<div class="grid-container menucontents">
				<a href="javascript:slideHelpUp('tips-box');">
                                        <div class="grid-25 help">
                                                <i class="fa fa-question-circle"></i>
                                        </div>
                                </a>
		</div>
	</menu>--}}
	@yield('content')
	</body>
</html>
