@extends('blank')
@section('title', 'No page')
@section('head')
<style>
body {
	overflow: hidden;
	background-color: white;
}
* {
	border: 0;
	padding: 0;
	margin: 0;
}
iframe {
	outline: none;
	border-width: 0;
	box-sizing: border-box;
	width: 100%;
	height: 100%;
}
</style>
<script>
	document._iframeHeight = 2000;
</script>
@endsection
@section('content')

Ingen side valgt

@endsection
