@extends('blank')
@section('title', $oembed->title)
@section('head')
<style>
body {
	overflow: hidden;
	background-color: white;
}
* {
	border: 0;
	padding: 0;
	margin: 0;
}
iframe {
	outline: none;
	border-width: 0;
	box-sizing: border-box;
	width: 100%;
	height: 100%;
}
</style>
@endsection
@section('content')
    @include('partials.display', array(
        'page' => $page,
        'oembed' => $oembed
    ))
@endsection
