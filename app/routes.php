<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

/* Not Editable Package Routes*/
/* Show view of uneditable package */
Route::get('package/{packageId}/', array('as' => 'package.get', 'uses' => 'FrontPackageController@getPackageView'));
/* Show Json of uneditable package */
Route::get('package/{packageId}/json', array('as' => 'package.get_json', 'uses' => 'FrontPackageController@getPackageJson'));
/* Show XML of uneditable package */
Route::get('package/{packageId}/xml', array('as' => 'package.get_xml', 'uses' => 'FrontPackageController@getPackageXML'));
/*translate a package*/
Route::get('package/{packageId}/translate/{lang}', array('as' => 'package.translate', 'uses' => 'PackageController@translatePackage'));


/* Editable Package Routes*/
/* Error */
Route::get('packages/error', array('as' => 'package.error', 'uses' => 'ErrorController@error'));
/* return the final url */
Route::get('packages/{packageId}/url', array('as' => 'package.url', 'uses' => 'PackageController@returnUrl'));
/* list all packages */
Route::get('packages/list', array('as' => 'package', 'uses' => 'PackageController@getIndex'));
/*create a package*/
Route::get('packages/create', array('as' => 'package.create', 'uses' => 'PackageController@create'));
/* edit a package */
Route::get('packages/{packageId}/page/{pos}/edit', array('as' => 'package.edit', 'uses' => 'PackageController@edit'));
/* view a package */
Route::get('packages/{packageId}/page/{pos}/preview', array('as' => 'package.view', 'uses' => 'PackageController@view'));
/* preview a package */
Route::get('packages/{packageId}/page/{pos}/', array('as' => 'package.preview', 'uses' => 'PackageController@preview'));
/* update a package */
Route::post('page/{pageId}/update', array('as' => 'package.update', 'uses' => 'PackageController@update'));
/* commit a package */
Route::post('packages/{packageId}/page/{pos}/save', array('as' => 'package.commit', 'uses' => 'PackageController@commit'));
/* save a draft of package */
Route::get('packages/{packageId}/draft', array('as' => 'package.draft', 'uses' => 'PackageController@saveAsDraft'));
/* duplicat a package */
Route::get('package/{packageId}/copy', array('as' => 'package.duplicate', 'uses' => 'PackageController@duplicatePackage'));
/* copy a package */
Route::get('package/{packageId}/edit', array('as' => 'package.copy', 'uses' => 'PackageController@copy'));
/* delete a page */
Route::get('page/{pos}/package/{packageId}/delete', array('as' => 'page.delete', 'uses' => 'PageController@delete'));
/*create a page*/
Route::get('page/{packageId}/create', array('as' => 'page.create', 'uses' => 'PageController@create'));
/*move a page*/
Route::get('page/{packageId}/{movedItem}/{movedPos}/{aboveItem}/{abovePos}/move', array('as' => 'page.move', 'uses' => 'PageController@move'));

Route::get('page/{pageId}/display', array('as' => 'page.display', 'uses' => 'PageController@display'));

/*login*/
Route::get('user/login', array('as' => 'user.login', 'uses' => 'UserController@login'));

/*logout*/
Route::get('user/logout', array('as' => 'user.logout', 'uses' => 'UserController@logout'));
/*oauth return*/
Route::get('user/oauthreturn.php', array('as' => 'user.oauthreturn', 'uses' => 'UserController@oauthReturn'));

Route::controller('admin', 'AdminController');
