<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePackageversionsPages extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('packageversions_pages', function($table)
		{
			$table->increments('id');
			$table->unsignedInteger('package_version_id');
			$table->unsignedInteger('page_id');
			$table->integer('pos');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('packageversions_pages');
	}

}
