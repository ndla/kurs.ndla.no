<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePackagesWithSoftdelete extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement("SET sql_mode = '';");
		Schema::table('packages', function(Blueprint $table)
		{
			$table->softDeletes();
		});
		Schema::table('package_versions', function(Blueprint $table)
		{
			$table->softDeletes();
		});
		Schema::table('pages', function(Blueprint $table)
		{
			$table->softDeletes();
		});
		

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('packages', function(Blueprint $table)
		{
			//
		});
	}

}
