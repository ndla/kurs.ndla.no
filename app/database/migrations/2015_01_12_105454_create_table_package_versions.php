<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePackageVersions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('package_versions', function($table)
		{
			$table->increments('id');
			$table->string('title');
			$table->boolean('commited');
			$table->unsignedInteger('parent_id')->nullable();
			$table->unsignedInteger('creator_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('package_versions');
	}

}
