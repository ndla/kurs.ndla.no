<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{	
		DB::statement("SET sql_mode = '';");
		Schema::table('users', function($table)
		{
			$table->string('username', 32);
          	$table->string('email', 320);
          	$table->string('password', 64);

			// required for Laravel 4.1.26
			$table->string('remember_token', 100)->nullable();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
