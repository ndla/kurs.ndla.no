<?php

class AdminController extends \BaseController {

	public function __construct() {
		$this->beforeFilter('auth', array('except' => ['getLogin','postLogin']));
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex($show_deleted = false)
	{
		$show_deleted = Input::get('show_deleted');
		
		if($show_deleted != null) {
			$deleted_packages = Package::withTrashed()->get();
			$packages = [];
			foreach($deleted_packages as $package) {
				$package_version = PackageVersions::withTrashed()->find($package->package_version_id);
				$package->package_versions = $package_version;
				
				
				$packages[] = $package;
			}
		} else {
			$packages = Package::with('package_versions')->get();	
		}
		
		
		return View::make('admin.index', ['packages' => $packages]);
	}

	public function getLogin() {
		return View::make('admin.login');
	}

	public function postLogin() {
		$username = Input::get('username');
		$password = Input::get('password');
		
		if($username == 'laweb' && md5($password) == "dac692446c6d91ad62d6ffd726950520") {
			Auth::loginUsingId(1);

      Session::put("user.validated", true);
      Session::put("user.id", 1);

      return Redirect::intended('admin/index');
		} 

		return Redirect::back()
			->withInput()
			->withErrors('That username/password combo does not exist.');
	}

	public function getLogout() {
		Auth::logout();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function getDelete($packageId) {
		$package = Package::find($packageId);
		if($package) {
			$package_version = PackageVersions::find($package->package_version_id);
			$package->delete();
			if($package_version) $package_version->delete();
		}
		
		
		if (Request::format() == 'json') {
			return Response::json(array('status' => 'ok'));	
		} 
		
		return Redirect::back();

	}

	public function getRestore($packageId) {
		$package = Package::withTrashed()->findOrFail($packageId);
		$pagage_version = PackageVersions::withTrashed()->findOrFail($package->package_version_id);
		$package->restore();
		$pagage_version->restore();
		

		if (Request::format() == 'json') {
			return Response::json(array('status' => 'ok'));	
		} 
		

		return Redirect::back();
	}

	public function getPublish($packageId) {
		$package = Package::with('package_versions')->findOrFail($packageId);
		$package->package_versions->commited = 1;
		$package->push();


		if (Request::format() == 'json') {
			return Response::json(array('status' => 'ok'));	
		} 
		
		return Redirect::back();
	
	}

	public function getUnpublish($packageId){
		$package = Package::with('package_versions')->findOrFail($packageId);
		$package->package_versions->commited = 0;
		$package->push();

		if (Request::format() == 'json') {
			return Response::json(array('status' => 'ok'));	
		} 
		

		return Redirect::back();
	}
}
