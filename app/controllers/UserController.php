<?php
class UserController extends \BaseController
{
	public function login()
	{
		
		require(app_path().'/library/AuthOAuth-master/authoauth.php');
		$config = require(app_path().'/library/AuthOAuth-master/exampleconfig.sample.php');
		$oauth = new no\seria\authoauth\AuthOAuth($config['hostUri'], $config['oauthId'], $config['oauthKey']);
		$oauth->setRedirectUri($config['redirectUri']);
		return Redirect::to($oauth->getAuthorizeUrl());

		// Session::set('identity', 1);
		// Session::set('user.id', 1);
		// Session::set('user.name', 'admin');		
		// return Redirect::route('package');

	}

	public function logout()
	{
		Session::forget('identity');
		Session::forget('user.id');
		Session::forget('user.name');
		return Redirect::route('package');
	}

	public function oauthReturn()
	{
		require(app_path().'/library/AuthOAuth-master/authoauth.php');
		$config = require(app_path().'/library/AuthOAuth-master/exampleconfig.sample.php');
		$oauth = new no\seria\authoauth\AuthOAuth($config['hostUri'], $config['oauthId'], $config['oauthKey']);
		$oauth->setRedirectUri($config['redirectUri']);
		Session::set('identity', $oauth->getIdentity());
		Session::set('user.id', $oauth->getIdentity());
		Session::set('user.name', $oauth->getIdentity());
		return Redirect::route('package');
	}
}
