<?php
class FrontPackageController extends \BaseController
{
	/* View Package */
	public function getPackageView($package_id)
	{
		//if not page number is passed, use the first page
		if(!isset($_GET['page']))
			$pos = 1;
		else
			$pos = $_GET['page'];
		//find the'root' package
		$packageP = Package::find($package_id);
		if(!$packageP['id']) {
			App::abort(404);
		}
		//find the id of latest version of the package
		$real_package_id = $packageP->package_version_id;
		//get the latest package
		$package = PackageVersions::find($real_package_id);
		//get all the pages in the latest package
		$pages = $package->pages;
		//get the page
		$page = PackageVersions::find($real_package_id)->pages()->where('pos', '=', $pos)->first();

		if (filter_var($page->url, FILTER_VALIDATE_URL)) {
      //rewrite the url
      if(strpos($page->url, 'red.ndla.no') !== false) {
        $page->url = str_replace("red.ndla.no", "ndla.no", $page->url);
      }
      else if(strpos($page->url, 'red.test.ndla.no') !== false) {
        $page->url = str_replace("red.test.ndla.no", "cm.test.ndla.no", $page->url);
      }

			$url = parse_url($page->url);
			$ndla_hosts = array("ndla.no", "red.ndla.no", "cm.test.ndla.no", "red.test.ndla.no", "red.ndla.l");
			if (in_array($url["host"], $ndla_hosts)) {
				$page->url = $url["scheme"] . "://" . $url["host"] . $url["path"];
      }

      if (Request::server('HTTP_X_FORWARDED_PROTO') == 'https' || ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off')
        || $_SERVER['SERVER_PORT'] == 443)) {
        $page->url = preg_replace("/^http:\/\//", "https://", $page->url);
      }

      try {
        $oembed = $page->getEmbed();
      } catch (Exception $e) {}
    }

    return View::make('package.studentview', compact('page', 'pages', 'package_id', 'pos', 'cards', 'package', 'oembed'));
	}

	public function getPackageJSON($packageId)
	{
		//find the package
		$package = Package::find($packageId);
		//get the latest version of that package
		$packageVersions = PackageVersions::find($package->package_version_id);
		//get the pages
		$pages = $packageVersions->pages;
		//build the package detail array
		$jsonArray = array('package_id' => $package->id,
					'package_version_id' => $package->package_version_id,
					'package_title' => $packageVersions->title,
					'package_created_by' => $packageVersions->creator_id,
					'package_created_date' => $packageVersions->created_at,
					'package_modified_date' => $packageVersions->updated_at,
					'published' => $packageVersions->commited,
					'hours' => $packageVersions->hours,
					'minutes' => $packageVersions->minutes);
		//add the pages to the array
		foreach($pages as $p)
		{
			$jsonArray['pages'][$p->id] = array('id'=>$p->id,
								'title'=>$p->title,
								'url'=>$p->url,
								'content'=>htmlentities($p->content),
								'type'=>$p->type,
								'pos'=>$p->pivot->pos);
		}
		//output the json
		echo json_encode($jsonArray);
	}

	public function getPackageXML($packageId)
	{
		//get the data
		$package = Package::find($packageId);
		$packageVersions = PackageVersions::find($package->package_version_id);
		$pages = $packageVersions->pages;
		$array = array('package_id' => $package->id,
					'package_version_id' => $package->package_version_id,
					'package_title' => $packageVersions->title,
					'package_created_by' => $packageVersions->creator_id,
					'package_created_date' => $packageVersions->created_at,
					'package_modified_date' => $packageVersions->updated_at,
					'published' => $packageVersions->commited );

		foreach($pages as $p)
		{
			$array['pages']['page'.$p->id] = array('id'=>$p->id,
								'title'=>$p->title,
								'url'=>$p->url,
								'content'=>htmlentities($p->content),
								'type'=>$p->type,
								'pos'=>$p->pivot->pos);
		}
		$xml = $this->array2xml($array);
	}

	function array2xml($array, $node_name="root")
	{
		$dom = new DOMDocument('1.0', 'UTF-8');
		$dom->formatOutput = true;
		$root = $dom->createElement($node_name);
		$dom->appendChild($root);
		$array2xml = function($node, $array) use ($dom, &$array2xml)
		{
			foreach($array as $key => $value)
			{
				if(is_array($value))
				{
					$n = $dom->createElement($key);
					$node->appendChild($n);
					$array2xml($n, $value);
				}
				else
				{
					$attr = $dom->createAttribute($key);
					$attr->value = $value;
					$node->appendChild($attr);
				}
			}
		};
		$array2xml($root, $array);
		return $dom->saveXML();
	}
}
