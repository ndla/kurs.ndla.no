<?php
class PackageController extends \BaseController
{
	public function __construct()
  {

		//check we have a valid user
		if(!Session::has('user.validated') || Session::get('user.validated') == false)
		{
			if(!isset($_GET['user']))
				return Redirect::route('package.error')->with('message', "You are not authorised to access this page")->send();
			if(!isset($_GET['timestamp']))
				return Redirect::route('package.error')->with('message', "You are not authorised to access this page")->send();
			$key = $_ENV['kurs-secret-key'];
			if(!isset($_GET['sig']))
				return Redirect::route('package.error')->with('message', "You are not authorised to access this page")->send();
			if($_GET['timestamp'] < time()-60)
				return Redirect::route('package.error')->with('message', "You are not authorised to access this page")->send();
			$sig = $_GET['sig'];
			$oursig = hash_hmac("sha256",$_GET['user']."red.ndla.no".$_GET['timestamp'], $key);
			if($oursig != $sig)
				return Redirect::route('package.error')->with('message', "You are not authorised to access this page")->send();
			else
			{
				$user = Users::where('externalId','=',$_GET['user'])->first();
				if(!isset($user))
				{
					//create a new user
					$userarr = array('externalId'=>$_GET['user']);
					$user = Users::create($userarr);
					$user->save();
				}
				Session::put("user.validated", true);
				Session::put("user.id", $_GET['user']);
			}
		}
	}

	/*Translate a package*/
	public function translatePackage($packageId, $lang)
	{
		$pos = 1;
		$packageP = Package::find($packageId);
		if(is_null($packageP))
		{
			$package = PackageVersions::find($packageId);
			if(is_null($package))
				return Redirect::route('package.error')->with('message', "An error has occurred, the requested package could not be found.")->send();
		}
		else
		{
			$packageId = $packageP->package_version_id;
			$package = PackageVersions::find($packageId);
		}
		if(is_null($package))
			 return Redirect::route('package.error')->with('message', "An error has occurred, the requested package could not be found.")->send();
		//build a json array of the pages
		$urlJSON = array();
		$pages = PackageVersions::find($packageId)->pages;
		$introText = '';
		$titlesArray = array();
		foreach($pages as $p)
		{
			if($p->pivot->pos == 1)
				$introText = $p->content;
			if($p->url != null)
				$urlJSON[] = array('url'=>$p->url,'pos'=>$p->pivot->pos,'type'=>$p->type);
			$titlesArray[$p->pivot->pos] = $p->title;
//			PackageVersions::find($package_version_insert->id)->pages()->attach($p->id, array('pos'=>$p->pivot->pos));
		}
		$mainJSON = array('lang' => $lang, 'pages' => $urlJSON);
		$translator = new Translate($mainJSON);
//		die(var_dump($translator->getContent()));
//		die(var_dump($mainJSON));
		$translatedArray = $translator->getContent();
		//create a new package version
		$package_version = array('title'=>$package->title,
					'creator_id'=>Session::get('user.id'),
					'hours'=>$package->hours,
					'minutes'=>$package->minutes,
					'commited'=>'false');
		$package_version_insert = PackageVersions::create($package_version);
		$package_version_insert->save();
		//add to the package 'holder' table
		$new_package = array('package_version_id' => $package_version_insert->id);
		$package_insert = Package::create($new_package);
		$package_insert->save();

		//now make all the associations
		$pages = PackageVersions::find($packageId)->pages;
		if($lang == 'nb')
			$introTitle = "Introduksjon";
		else if($lang == 'nn')
			$introTitle = "Introduksjon";
		else
			$introTitle = "Introduction";
		$intro = array('content'=>$introText,
				'title'=>$introTitle,
				'creator_id'=>Session::get('user.id'),
				'type'=>1);
		$page = Page::create($intro);
		$page->save();
		PackageVersions::find($package_version_insert->id)->pages()->attach($page->id, array('pos'=>1));

		foreach($translatedArray as $p)
		{
			//create the pages
			$pageArray = array();
			$pageArray['url'] = $p->url;

			if(isset($titlesArray[$p->pos]) && strlen($titlesArray[$p->pos]) > 0)
				$title = $titlesArray[$p->pos];

			if(isset($title) && strlen($title)>0)
				$pageArray['title'] = $title;
			else
				$pageArray['title'] = 'New Page';
			if(isset($p->type))
				$pageArray['type'] = $p->type;
			else
				$pageArray['type'] = 2;
			$pageArray['creator_id'] = Session::get('user.id');
			$npage = Page::create($pageArray);
			$npage->save();

			PackageVersions::find($package_version_insert->id)->pages()->attach($npage->id, array('pos'=>$p->pos));
		}
		return Redirect::route('package.edit', array('packageId'=>$package_version_insert->id, 'pos'=>1));

	}

	/*Duplicate a package*/
	public function duplicatePackage($packageId)
	{
		$pos = 1;
		$packageP = Package::find($packageId);
		if(is_null($packageP))
		{
			$package = PackageVersions::find($packageId);
			if(is_null($package))
				return Redirect::route('package.error')->with('message', "An error has occurred, the package could not be duplicated.")->send();
		}
		else
		{
			$packageId = $packageP->package_version_id;
			$package = PackageVersions::find($packageId);
		}
		if(is_null($package))
			 return Redirect::route('package.error')->with('message', "An error has occurred, the package could not be duplicated.")->send();
		//create a new package version
		$package_version = array('title'=>$package->title,
					'creator_id'=>Session::get('user.id'),
					'commited'=>'false');
		$package_version_insert = PackageVersions::create($package_version);
		$package_version_insert->save();
		//add to the package 'holder' table
		$new_package = array('package_version_id' => $package_version_insert->id);
		$package_insert = Package::create($new_package);
		$package_insert->save();

		//now make all the associations
		$pages = PackageVersions::find($packageId)->pages;
		foreach($pages as $p)
		{
			//make a copy of the intro
			if($p->type == 1)
			{
				$intro = array('content'=>$p->content,
						'title'=>$p->title,
						'creator_id'=>Session::get('user.id'),
						'type' => 1);
				$page = Page::create($intro);
				$page->save();
				PackageVersions::find($package_version_insert->id)->pages()->attach($page->id, array('pos'=>1));
			}
			else
			{
				//create copies of the pages
				PackageVersions::find($package_version_insert->id)->pages()->attach($p->id, array('pos'=>$p->pivot->pos));
			}
		}
		return Redirect::route('package.edit', array('packageId'=>$package_version_insert->id, 'pos'=>$pos));
	}

	//create an editable copy of the package
	public function copy($packageId)
	{
		$pos = 1;
		$packageP = Package::find($packageId);
		if(is_null($packageP))
		{
			$package = PackageVersions::find($packageId);
			if(is_null($package))
				return Redirect::route('package.error')->with('message', "An error has occurred, the package could not be copied.")->send();

		}
		else
		{
			$packageId = $packageP->package_version_id;
			$package = PackageVersions::find($packageId);
		}
		if(is_null($package))
			 return Redirect::route('package.error')->with('message', "An error has occurred, the package could not be copied.")->send();
		if($package->commited == 0)
			return Redirect::route('package.edit', array('packageId'=>$packageId, 'pos'=>$pos));
		//create a new package version
		$package_version = array('title'=>$package->title,
					'creator_id'=>Session::get('user.id'),
					'commited'=>'false',
					'parent_id'=>$packageId);
		$package_version_insert = PackageVersions::create($package_version);
		$package_version_insert->save();
		//now make all the associations
		$pages = PackageVersions::find($packageId)->pages;
		foreach($pages as $p)
		{
			//make a copy of the intro
			if($p->type == 1)
			{
				$intro = array('content'=>$p->content,
						'title'=>$p->title,
						'creator_id'=>Session::get('user.id'),
						'type' => 1);
				$page = Page::create($intro);
				$page->save();
				PackageVersions::find($package_version_insert->id)->pages()->attach($page->id, array('pos'=>1));
			}
			else
			{
				PackageVersions::find($package_version_insert->id)->pages()->attach($p->id, array('pos'=>$p->pivot->pos));
			}
		}
		return Redirect::route('package.edit', array('packageId'=>$package_version_insert->id, 'pos'=>$pos));
	}

	/* Creating a NEW package */
	public function create()
	{
		//setup the data for the package version
		$package_version = array('title'=>'Ny læringssti',
					'creator_id'=>Session::get('user.id'),
					'commited'=>'false');
		//create the new package version
		$package_version_insert = PackageVersions::create($package_version);
		$package_version_insert->save();
		//setup the data for the package
		$package = array('package_version_id'=>$package_version_insert->id);
		//create entry in the package table (as it is the only package it automatically becomes the default version)
		$package_insert = Package::create($package);
		$package_insert->save();
		//setup the data for the introduction page
		$page = array('package_version_id'=>$package_version_insert->id,
				'title'=>'Introduksjon',
				'pos'=>1,
				'type'=>1,
				'creator_id'=>Session::get('user.id'));
		//create the introduction page
		$page_insert = Page::create($page);
		$page_insert->save();
		//create the assoc row
		$assoc = array('package_version_id' => $package_version_insert->id,
				'pos' => 1,
				'page_id' => $page_insert->id);
		$pp_assoc = PackageVersionPageAssoc::create($assoc);
		$pp_assoc->save();
		//setup the data for the final page
		$page = array('package_version_id'=>$package_version_insert->id,
				'title'=>'Test',
				'pos'=>2,
				'type'=>8,
				'creator_id'=>Session::get('user.id'));
		//create the blank page
		$page_insert = Page::create($page);
		$page_insert->save();
		$assoc = array('package_version_id' => $package_version_insert->id,
				'pos' => 2,
				'page_id' => $page_insert->id);
		$pp_assoc = PackageVersionPageAssoc::create($assoc);
		$pp_assoc->save();
		//redirect the user to the introduction page editor
		return Redirect::route('package.edit', array('packageId' => $package_version_insert->id, "pos"=>1));
	}

	/* Setup the edit package page */
	public function edit($package_id, $pos)
	{
		$package = PackageVersions::find($package_id);
		if(!isset($package))
			return Redirect::route('package.error')->withMessage("Package not found");
		//if the package has been commited no more edits can be made
		if($package->commited == 1)
		{
			return Redirect::route('package.preview', array('packageId'=>$package_id, 'pos'=>$pos));
		}
		else
		{
			$pages = PackageVersions::find($package_id)->pages;
			$page = PackageVersions::find($package_id)->pages()->where('pos', '=', $pos)->first();
			if(!isset($page))
				return Redirect::route('package.error')->withMessage("Page not found");
      if(filter_var($page->url, FILTER_VALIDATE_URL)) {
      try {
        $oembed = $page->getEmbed();
      } catch (Exception $e) {}
    }
			$view = 0;
			if($pos == 1 && strlen($page->content)==0)
				$newpackage = 1;
			else
				$newpackage = 0;
			return View::make('package.edit', compact('page', 'pages', 'package_id', 'pos', 'cards', 'view', 'newpackage', 'package', 'oembed'));
		}
	}
	/* Setup the view package page */
	public function view($package_id, $pos)
	{
		$package = PackageVersions::find($package_id);
		if(!isset($package))
			return Redirect::route('package.error')->withMessage("Package not found");
		//if the package has been commited no more edits can be made
		if($package->commited == 1)
		{
			return Redirect::route('package.preview', array('packageId'=>$package_id, 'pos'=>$pos));
		}
		$pages = PackageVersions::find($package_id)->pages;
		$page = PackageVersions::find($package_id)->pages()->where('pos', '=', $pos)->first();
		if(!isset($page))
			return Redirect::route('package.error')->withMessage("Page not found");
    if(filter_var($page->url, FILTER_VALIDATE_URL)) {
      try {
        $oembed = $page->getEmbed();
      } catch (Exception $e) {}

    }
		$view = 1;
		$newpackage = 0;
		return View::make('package.edit', compact('page', 'pages', 'package_id', 'pos', 'cards', 'view', 'newpackage', 'package', 'oembed'));
	}
	/* Setup the preview package page */
	public function preview($package_id, $pos) {
		$package = PackageVersions::find($package_id);
		if(!isset($package))
			return Redirect::route('package.error')->withMessage("Package not found");
		
		if($package->commited == 0) {
			return Redirect::route('package.view', array('packageId'=>$package_id, 'pos'=>$pos));
		} else {
			$pages = PackageVersions::find($package_id)->pages;
			$page = PackageVersions::find($package_id)->pages()->where('pos', '=', $pos)->first();
			if(!isset($page))
				return Redirect::route('package.error')->withMessage("Page not found");
			
      if(filter_var($page->url, FILTER_VALIDATE_URL)) {
	      try {
	        $oembed = $page->getEmbed();
	      } catch (Exception $e) {}
    	}
			return View::make('package.preview', compact('page', 'pages', 'package_id', 'pos', 'cards', 'oembed'));
		}
	}

	public function commit($packageId, $pos)
	{
		$input = Input::all();
		$pv = PackageVersions::find($packageId);
		$pv->title = $input['packagetitle'];
		$pv->hours = $input['hours'];
		$pv->minutes = $input['minutes'];
		$pv->save();
		if($input['status'] == 'd')
		{
			return Redirect::route('package.draft', array('packageId' => $packageId));
		}
		$realPID = $packageId;
		$pages = PackageVersions::find($packageId)->pages;
		//first check the rules are followed
		//1. is there an intro
		//2. are there any blank pages
		$broken = false;
		foreach($pages as $p)
		{
			if($p->type == 1 && strlen($p->content)==0)
			{
				$msg = "En pakke m&aring; inneholde en introduksjon.";
				$broken = true;
				$pos = $p->pivot->pos;
			}
			else if($p->type == 0 || ($p->type != 1 && strlen($p->url)==0))
			{
				$msg = "Alle sider i pakken m&aring; inneholde sidetype og url.";
				$broken = true;
				$pos = $p->pivot->pos;
			}
		}
		if(!$broken)
		{
			$p = PackageVersions::find($packageId);
			$p->commited = 1;
			$p->save();
			//need to update the Package table to point to this package
			$packageId = $p->parent_id;
			if(!is_null($packageId) && isset($packageId))
			{
				$packageFinal = Package::where('package_version_id','=',$packageId)->first();
				$packageFinalId = $packageFinal->id;
				$pp = Package::find($packageFinal->id);
				$pp->package_version_id = $realPID;
				$pp->save();
			}
			return Redirect::route('package.url', array('packageId' => $realPID));
		}
		else
		{
			//do some error stuff here
			return Redirect::route('package.edit', array('packageId' => $packageId, 'pos'=>$pos))->with('message', $msg);
		}
	}

	public function saveAsDraft($packageId)
	{

		//do we have a record in the parent table?
		$packageFinal = Package::where('package_version_id','=',$packageId)->first();
		//this package was created as a dupe, we need to create the package record
		if(is_null($packageFinal))
		{
			$package = array('package_version_id'=>$packageId);
	                //create entry in the package table (as it is the only package it automatically becomes the default version)
        	        $package_insert = Package::create($package);
			$packageFinal = Package::where('package_version_id','=',$packageId)->first();
		}
		return Redirect::route('package.url', array('packageId' => $packageId));
	}

public function update($pageId)
	{
		$input = Input::all();
		$packageId = $input['package_id'];
		$pv = PackageVersions::find($packageId);
		$pv->title = $input['packagetitle'];
		$pv->hours = $input['packagehours'];
		$pv->minutes = $input['packageminutes'];
		$pv->save();

		$pos = $input['pos'];
		$page = Page::find($pageId);
		//if we have a content editor then we are on the intro page
		//we can do a straight save
		if(isset($input['content-editor']) && !isset($input['url']))
		{
			$page->content = $input['content-editor'];
			$page->save();
			return Redirect::route('package.edit', array('packageId'=>$packageId, "pos"=>$pos));
		}
		else
		{
			//and a new row for the assoc
			//if the url has been set, and it doesn't equal whats in the db, see if we can find a match in the db for that
			//url and type
			if((isset($input['url']) && $input['url'] != $page->url) 
				|| (isset($input['type']) && $input['type'] != $page->type)
				|| (isset($input['title']) && $input['title'] != $page->title)
				|| (isset($input['content-editor']) && $input['content-editor'] != $page->content))
			{
				$npage = Page::where('url','=',$input['url'])->where('type','=',$input['type'])->where('title','=',$input['title'])->where('content','=',$input['content-editor'])->first();
				//if we got a matching row then change the assoc
				if($npage)
				{
					$pvp = PackageVersionPageAssoc::where('package_version_id', '=', $packageId)->where('page_id', '=', $pageId)->where("pos","=",$pos)->first();
					$pvp->page_id = $npage->id;
					$pvp->save();
					return Redirect::route('package.edit', array('packageId'=>$packageId, 'pos'=>$pos));
				}
				//otherwise create a new page
				else
				{
						$pageArray = array();

						if(strpos($input['url'], 'http://') !== 0 && strpos($input['url'], 'https://') !== 0 && empty($input['url']) !== TRUE)
						{
							$input['url'] = 'http://'.$input['url'];
						}

                        // Rewrite NDLA URLs to `/node/{nid}` before save
                        if(!empty($input['url'])) {
                            $url = parse_url($input['url']);
                            $ndla_hosts = array("ndla.no", "red.ndla.no", "cm.test.ndla.no", "red.test.ndla.no", "red.ndla.l");
                            if (in_array($url["host"], $ndla_hosts)) {
                                $input['url'] = preg_replace('/(\/\w{2})?\/\w{1,}\//', '/node/', $input['url']);
                                $input['url'] = preg_replace('/\?(.*)/', '', $input['url']);
                            }
                        }

						$pageArray['url'] = $input['url'];
						
						if(isset($input['title']) && strlen($input['title'])>0)
							$pageArray['title'] = $input['title'];
						else
							$pageArray['title'] = 'New Page';

						if(isset($input['type']))
							$pageArray['type'] = $input['type'];
						else
							$pageArray['type'] = 2;

						$pageArray['content'] = $input['content-editor'];
						$pageArray['creator_id'] = Session::get('user.id');
						$npage = Page::create($pageArray);
						$npage->save();
						$pvp = PackageVersionPageAssoc::where('package_version_id', '=', $packageId)
							->where('page_id', '=', $pageId)
							->where('pos','=',$pos)
							->first();

						PackageVersions::find($packageId)->pages()->attach($npage->id, array('pos'=>$pvp->pos));
						$pvp->delete();
						return Redirect::route('package.edit', array('packageId'=>$packageId, "pos"=>$pos));
				}
			}
		}
		return Redirect::route('package.edit', array('packageId'=>$packageId, "pos"=>$pos));
	}

	//list all the pages within a package
	public function getIndex()
	{
		$package = PackageVersions::where('commited','=','0')->get();
		return View::make('package.list', compact('package'));
	}

	public function details($id)
	{
		return View::make('package.details', compact('id'));
	}

	/*return the final url that points to the latest version of a package*/
	public function returnUrl($packageId)
	{
		$packageFinal = Package::where('package_version_id','=',$packageId)->first();
		if(is_null($packageFinal))
			 return Redirect::route('package.error')->with('message', "An error has occurred, the package could not be found.")->send();
		$packageFinalId = $packageFinal->id;
		$packageVersion = PackageVersions::find($packageId);
		if(is_null($packageVersion))
			 return Redirect::route('package.error')->with('message', "An error has occurred, the package could not be found.")->send();
		$packageFinalTitle = $packageVersion->title;
		return View::make('package.finalurl', compact('packageFinalId', 'packageFinalTitle'));
	}

	public function getPackageId($packageId)
	{
		$package = PackageVersions::where('id', '=', $packageId)->first();
		if(!is_null($package->parent_id))
			return $this->getPackageId($package->parent_id);
		else
			return $package->id;
	}
}
