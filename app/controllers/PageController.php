<?php
/**
*	Responsible for /page/{pageId}-urls
*/
class PageController extends \BaseController
{
        public function __construct()
        {

        }

	/* View a page */
	public function display($page_id) {
		// 1. Load the page model
		$page = Page::find($page_id);
		// 2. Parse the embed code stuffy
		try {
			$oembed = $page->getEmbed();
		} catch (Exception $e) {

			return View::make('page.empty');
		}

		// 3. Render the view
		return View::make('page.display', array(
			'page' => $page,
			'oembed' => $oembed,
		));
	}

	/*create a new page*/
	public function create($package_id)
	{
		//get the max pos
		$p = PackageVersions::find($package_id)->pages->reverse()->first();
		//if we didn't find anything show an error
		if(is_null($p))
		{
			 return Redirect::route('package.error')->with('message', "The package could not be found")->send();
		}
		else
		{
			$pos = $p->pivot->pos;
			//check if we have a final test
			if($p->type == 8)
			{
				//increase the pos of the final test so it is always the last page
				$p = PackageVersionPageAssoc::where('package_version_id','=',$package_id)->where('pos','=',$pos)->first();
				$p->pos = $p->pos+1;
				$p->save();
			}
			else
			{
				//if not final test, increase the new pos by one
				$pos = $pos+1;
			}
		}
		//see if we have a 'blank'page
		$page = Page::where('type','=','0')->whereNull('url')->whereNull('content')->first();
		if(!$page)
		{
			//No blank page, create one and add assoc
			$page = array('title'=>'New Page',
					'type' => 0,
					'creator_id' => Session::get('user.id'));
			$page_insert = Page::create($page);
			$page_insert->save();
			$package = PackageVersions::find($package_id);
			$package->pages()->attach($page_insert->id, array('pos' => $pos));
			return Redirect::route('package.edit', array('packageId'=>$package_id, 'pos'=>$pos));
		}
		else
		{
			//Add assoc to the blank page
			$package = PackageVersions::find($package_id);
			$package->pages()->attach($page->id, array('pos' => $pos));
			return Redirect::route('package.edit', array('packageId'=>$package_id, 'pos'=>$pos));
		}
	}

	public function delete($pos, $package_id)
	{
		//get the assoc to delete
		$p = PackageVersionPageAssoc::where('package_version_id','=',$package_id)->where('pos','=',$pos)->first();
		$p->delete();
		//need to adjust all the pos greater than the deleted page
		$pv = PackageVersionPageAssoc::where('package_version_id', '=', $package_id)->where('pos','>',$pos)->get();
		foreach($pv as $p)
		{
			$p->pos = $p->pos-1;
			$p->save();
		}

		return Redirect::route('package.edit', array('packageId'=>$package_id, 'pos'=>1));
	}

	public function move($package_id, $fromId, $fromPos, $toId, $toPos)
	{
		//moving item to the bottom
		if($toId == 0)
		{
			$pvas = PackageVersionPageAssoc::where('package_version_id','=',$package_id)->where('page_id','=',$fromId)->where('pos','=',$fromPos)->first();
			$pid = $pvas->id;
			$maxpos = 0;
			//shift the positions up by 1 til we reach the from item
			$pva = PackageVersionPageAssoc::where('package_version_id', '=',$package_id)->where('pos','>',$fromPos)->orderBy('pos', 'desc')->get();
			$count = 0;
			foreach($pva as $p)
			{
				//if we have a final test don't shift it
				$finaltest = Page::find($p->page_id);
				if($finaltest->type != 8)
					$count++;
				if($count != 0)
				{
					if($p->pos > $maxpos)
						$maxpos = $p->pos;
					$p->pos = $p->pos - 1;
					$p->save();
				}
				$count++;
			}
			//change the pos of the from item to the max available pos
			$pvas = PackageVersionPageAssoc::find($pid);
			$pvas->pos = $maxpos;
			$pvas->save();
			echo json_encode(array("url"=>"/packages/".$package_id."/page/".$maxpos."/edit"));
		}
		//moving item to new position
		else
		{
			//moving an item up the list
			if($fromPos > $toPos)
			{
				$pvas = PackageVersionPageAssoc::where('package_version_id','=',$package_id)->where('page_id','=',$fromId)->where('pos','=',$fromPos)->first();
				$pid = $pvas->id;
				$pva = PackageVersionPageAssoc::where('package_version_id', '=',$package_id)
								->where('pos','<',$fromPos)
								->where('pos','>=',$toPos)
								->orderBy('pos', 'desc')->get();
				foreach($pva as $p)
				{
					$p->pos = $p->pos + 1;
					$p->save();
				}
				//change the pos of the from item to the to pos
				$pvas = PackageVersionPageAssoc::find($pid);
				$pvas->pos = $toPos;
				$pvas->save();
				echo json_encode(array("url"=>"/packages/".$package_id."/page/".$toPos."/edit"));
			}
			//moving item down the list
			else
			{
				//shift all the items
				$pvas = PackageVersionPageAssoc::where('package_version_id','=',$package_id)->where('page_id','=',$fromId)->where('pos','=',$fromPos)->first();
				$pid = $pvas->id;
				$pva = PackageVersionPageAssoc::where('package_version_id', '=',$package_id)
								->where('pos','>',$fromPos)
								->where('pos','<',$toPos)
								->orderBy('pos', 'desc')->get();
				foreach($pva as $p)
				{
					$p->pos = $p->pos - 1;
					$p->save();
				}
				//update the from pos to be 1 item abve the to pos
				$pvas = PackageVersionPageAssoc::find($pid);
				$pvas->pos = $toPos-1;
				$pvas->save();

				echo json_encode(array("url"=>"/packages/".$package_id."/page/".($toPos-1)."/edit"));
			}
		}
	}

}
